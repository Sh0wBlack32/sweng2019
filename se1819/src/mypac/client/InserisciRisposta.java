package mypac.client;

import java.util.Date;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

import mypac.shared.Domanda;

/**
 * Classe che rappresenta l'interfaccia grafica che permette
 * l'inserimento di una nuova risposta
 *
 */
public class InserisciRisposta{
	private Label lblDomandaTxt;
	private TextArea txtRisposta;
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private Domanda domandaLoc;
	private Grid gridUrl;
	private VerticalPanel mainPanel;
	private ClickHandler btnRispostaHandler, btnAddUrlHandler, btnRemoveUrlHandler;

	/**
	 * Metodo che consente di popolare il pannello principale
	 * @param panel il pannello principale
	 * @param domanda la domanda a cui l'utente vuole inserire una riposta
	 */
	public void onModuleLoad(VerticalPanel panel, Domanda domanda) {
		
		//controllo: solo gli utenti registrati possono inserire risposte
		if(Se1819.utenteConnesso == null || Se1819.utenteConnesso.isEmpty()) {
			Window.alert("Non si dispone dei permessi necessari");
			Home h = new Home();
			h.onModuleLoad(panel);
			return;
		}
		mainPanel = panel;
		
		creazioneHandlerPulsanti();
		
		//creazione degli elementi dell'interfaccia grafica
		domandaLoc = domanda;
		Button btnR = new Button("Aggiungi risposta", btnRispostaHandler);
		lblDomandaTxt = new Label(domanda.getTesto());
		txtRisposta = new TextArea();
		HTML titolo = new HTML("<h1> Inserisci una risposta: </h1>");
		HTML allegato = new HTML("<p>Inserisci un eventuale allegato:</p>");
		Grid grid2 = new Grid(1,2);
		Button btnAddUrl = new Button("Aggiungi Url", btnAddUrlHandler);
		Button btnRemUrl = new Button("Rimuovi Url", btnRemoveUrlHandler);
		grid2.setWidget(0, 0, btnAddUrl);
		grid2.setWidget(0, 1, btnRemUrl);
		
		//aggiunta degli elementi al pannello principale
		mainPanel.add(titolo);
		mainPanel.add(lblDomandaTxt);
		mainPanel.add(txtRisposta);
		mainPanel.add(allegato);
		mainPanel.add(grid2);
		
		gridUrl = new Grid(0,1);
		mainPanel.add(gridUrl);
		mainPanel.add(btnR);
	}

	/**
	 * Metodo che si occupa della creazione dei ClickHandler 
	 * associati ai pulsanti per l'inserimento della risposta,
	 * l'aggiunta di un url e la rimozione di un url
	 */
	private void creazioneHandlerPulsanti() {
		
		//clickhandler associato al pulsante per l'inserimento di una nuova risposta
		btnRispostaHandler = new ClickHandler() {
			
			public void onClick(ClickEvent e) {
				String testo = txtRisposta.getText().trim();
				//controllo che il campo testo non sia vuoto e che abbia almeno 6 caratteri
				if (testo == null || testo.isEmpty()) {
					Window.alert("Il campo testo e' vuoto \n");
					return;
				}
				if (testo.length() < 6) {
					Window.alert("Il campo testo deve contenere almeno 6 caratteri \n");
					return;
				}
				//inserimento della risposta nel database
				greetingService.inserisciRisposta(domandaLoc, txtRisposta.getText().trim(), new Date(), Se1819.utenteConnesso, new AsyncCallback<String>() {
					//caso di insuccesso
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("C'e' stato un errore. "
								+ "\nErrore: " + caught.getMessage());
					}
					//caso di successo
					@Override
					public void onSuccess(String result) {
						Window.alert(result);
						//si torna alla schermata principale
						mainPanel.clear();
						Home h = new Home();
						h.onModuleLoad(mainPanel);
					}
				});
			}
		};

		//clickhandler associato al pulsante che permette l'aggiunta di un url
		btnAddUrlHandler = new ClickHandler() {
			
			public void onClick(ClickEvent e) {
				//creazione nuovi widget
				TextArea txtUrl = new TextArea();
				txtUrl.setCharacterWidth(80);
				txtUrl.setVisibleLines(1);
				//inserimento della nuova riga con i widget creati
				int r = gridUrl.getRowCount();
				gridUrl.insertRow(r);
				gridUrl.setWidget(r, 0, txtUrl);

			}
		};

		//clickhandler associato al pulsante che permette la rimozinoe di un url
		btnRemoveUrlHandler = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				//rimozione dell'ultima riga
				int row = gridUrl.getRowCount() - 1;
				gridUrl.removeRow(row);
			}
		};
	}
}
