package mypac.client;

/***
 * 
 * Classe che implementa l'inserimento di una nuova domanda nel sistema da parte dell'utente loggato
 * 
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import mypac.shared.Allegato;
import mypac.shared.Categoria;
import mypac.shared.Risposta;

/**
 * Classe che rappresenta la schermata per l'inserimento di una domanda
 *
 */
public class InserisciDomanda {
	private VerticalPanel mainPanel;
	private TextArea ta;
	private Button btn;
	private ClickHandler btnDomandaHandler;
	private ListBox listBox;

	private Categoria[] elencoCategorie;

	private Grid gridUrl;
	private ClickHandler btnAddUrlHandler, btnRemoveUrlHandler;

	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	/**
	 * Creazione dell'interfaccia grafica 
	 * @param panel
	 */
	public void onModuleLoad(VerticalPanel panel) {

		//Se l'utente non e' loggato
		if(Se1819.utenteConnesso == null || Se1819.utenteConnesso.isEmpty()) {
			//Messaggio d'errore
			Window.alert("Non si dispone dei permessi necessari");
			//Redirect alla home
			Home h = new Home();
			h.onModuleLoad(panel);
			return;
		}
		//Handler buttons
		creazioneHandlerPulsanti();

		//Panel
		mainPanel = panel;

		//Textarea
		ta = new TextArea();
		ta.setCharacterWidth(80);
		ta.setVisibleLines(10);
		ta.setEnabled(false);

		//Listbox che contiene le categorie possibili
		listBox = new ListBox();
		//Richiedo al server di restituire le categorie
		greetingService.restituisciCategorie(new AsyncCallback<Categoria[]>() {

			//Caso di fallimento
			@Override
			public void onFailure(Throwable caught) {
				//Messaggio d'errore
				Window.alert("C'e' stato un errore" + caught.getMessage());
			}

			//Caso di successo
			@Override
			public void onSuccess(Categoria[] result) {
				//Popolamento listbox con i nomi delle categorie
				elencoCategorie = result;
				for (Categoria c: result)
					listBox.addItem(c.getNomeCategoria());
			}
		});

		//Button "inserisci domanda"
		btn = new Button("Inserisci domanda", btnDomandaHandler);
		btn.setEnabled(false);

		//Titolo
		HTML titolo = new HTML("<h1>Inserisci una domanda:</h1>");
		HTML allegato = new HTML("<p>Inserisci un eventuale allegato:</p>");

		//Gestione panel e popolamento
		mainPanel.setWidth("100%");
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		mainPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		mainPanel.add(titolo);
		mainPanel.add(new Label("Inserisci il testo della domanda"));
		mainPanel.add(ta);

		//Grid
		Grid grid = new Grid(1,2);
		grid.setWidget(0, 0, new Label("Categoria"));
		grid.setWidget(0, 1, listBox);
		//Popolamento panel
		mainPanel.add(grid);
		mainPanel.add(allegato);
		//Gestione grid
		Grid grid2 = new Grid(1,2);

		//Buttons
		Button btnAddUrl = new Button("Aggiungi Url", btnAddUrlHandler);
		Button btnRemUrl = new Button("Rimuovi Url", btnRemoveUrlHandler);
		btnAddUrl.setEnabled(false);
		btnRemUrl.setEnabled(false);
		//Se e' loggato un utente o un admin
		if(Se1819.isUtente || Se1819.utenteConnesso.equalsIgnoreCase("admin")) {
			//Textarea e bottoni abilitati
			ta.setEnabled(true);
			btnAddUrl.setEnabled(true);
			btnRemUrl.setEnabled(true);
			btn.setEnabled(true);
		}
		//Popolamento grid
		grid2.setWidget(0, 0, btnAddUrl);
		grid2.setWidget(0, 1, btnRemUrl);
		//Popolamento panel
		mainPanel.add(grid2);
		//Grid per url
		gridUrl = new Grid(0,1);
		mainPanel.add(gridUrl);
		mainPanel.add(btn);
	}

	/**
	 * Metodo che crea gli handler dei pulsanti
	 */
	private void creazioneHandlerPulsanti() {
		//Handler del button inserisci domanda
		btnDomandaHandler = new ClickHandler() {
			//Gestione evento onClick
			@Override
			public void onClick(ClickEvent e) {
				String testo = ta.getText().toString().trim();
				Categoria categoria = elencoCategorie[listBox.getSelectedIndex()];
				//Lettura allegati
				List<Allegato> allegati = new ArrayList<Allegato>();
				for (int i = 0; i < gridUrl.getRowCount(); i++) {
					String url = ((TextArea) gridUrl.getWidget(i, 0)).getText().trim();
					if ((url != null) && (!url.isEmpty()))
						allegati.add(new Allegato(url));
				}

				//Creazione arraylist di risposte vuote in quanto la domanda nuova non ha risposte
				List<Risposta> risposte = new ArrayList<Risposta>(); 
				Date data = new Date();
				//Se il testo della domanda e' vuoto, messaggio d'errore
				if (testo == null || testo.isEmpty()) {
					Window.alert("Attenzione, la domanda non puo' essere vuota!");
					return;
				}

				String nomeUtente = Se1819.utenteConnesso;

				//Richiedo al server di effettuare la domanda
				greetingService.effettuaDomanda(testo, categoria, allegati, risposte, data, nomeUtente, new AsyncCallback<String>() {

					//Caso di fallimento
					@Override
					public void onFailure(Throwable caught) {
						//Messaggio d'errore
						Window.alert("c'e' stato un errore" + caught.getMessage());
					}

					//Caso di successo
					@Override
					public void onSuccess(String result) {
						Window.alert(result);
						//Una volta effettuata la domanda l'utente ha un redirect sulla home
						mainPanel.clear();
						Home home = new Home();
						home.onModuleLoad(mainPanel);
					}
				});
			}
		};

		//Handler bottone "add url"
		btnAddUrlHandler = new ClickHandler() {
			//Gestione evento onClick
			public void onClick(ClickEvent e) {
				//Creazione textarea
				TextArea txtUrl = new TextArea();
				txtUrl.setCharacterWidth(80);
				txtUrl.setVisibleLines(1);
				int r = gridUrl.getRowCount();
				gridUrl.insertRow(r);
				gridUrl.setWidget(r, 0, txtUrl);
			}
		};

		//Handler bottone "remove url"
		btnRemoveUrlHandler = new ClickHandler() {
			//Gestione evento onClick
			@Override
			public void onClick(ClickEvent event) {
				//Rimozione dell'ultima riga
				int row = gridUrl.getRowCount() - 1;
				gridUrl.removeRow(row);
			}
		};
	}
}
