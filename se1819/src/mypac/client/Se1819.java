package mypac.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * 
 * Classe che implementa la creazione del pannello principale e della menu bar visibile nella parte 
 * superiore dell'applicazione web
 * 
 */
public class Se1819 implements EntryPoint {

	public static String utenteConnesso = "";
	private static VerticalPanel mainPanel = new VerticalPanel();
	public static MyMenuBar menuBar;
	public static boolean isGiudice, isUtente;
	
	/**
	 * Creazione dell'interfaccia grafica 
	 */
	public void onModuleLoad() {
		//Creazione della barra del menu nella parte superiore del mainPanel
		menuBar = new MyMenuBar(mainPanel);
		//Aggiunta barre dei menu nella parte superiore
		menuBar.addNavHome();
		menuBar.addNavLogin();
		menuBar.addNavRegistrazione();
		menuBar.addNavNuovaDom();
		menuBar.addNavCategoria();
		menuBar.addNavNominaGiudice();
		menuBar.addNavLogout();
		menuBar.addNavProfilo();
		
		//Creazione del main panel
		mainPanel.setWidth("100%");
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		mainPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_TOP);
		//Aggiunta di barra e main panel al rootpanel
		RootPanel.get().add(menuBar.getMenuBar());
		RootPanel.get().add(mainPanel);
		//Caricamento della home
		Home h = new Home();
		h.onModuleLoad(mainPanel);
	}
}
