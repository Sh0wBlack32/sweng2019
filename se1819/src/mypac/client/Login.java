package mypac.client;



import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import mypac.client.Se1819;

/***
 * 
 * Classe che implementa la pagina per effettuare il login alla piattaforma
 * 
 */
public class Login {

	private Label lblNomeUtente, lblPassword, lblWelcome, lblWelcomeU;
	private TextBox txtNomeUtente;
	private PasswordTextBox txtPassword;

	private Grid grid;

	private VerticalPanel mainPanel;
	private ClickHandler btnLoginHandler;

	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	/**
	 * Creazione dell'interfaccia grafica mediante il popolamento del pannello
	 * @param panel
	 */
	public void onModuleLoad(VerticalPanel panel) {
		creazioneHandlerPulsanti();
		//Panel
		mainPanel = panel;
		//Griglia
		grid = new Grid(3, 2);
		//Titolo
		HTML title = new HTML("<h1>Login</h1>");

		//Etichette del form
		lblNomeUtente = new Label("Nome Utente:");
		lblPassword = new Label("Password:");
		lblWelcome = new Label("");
		lblWelcomeU = new Label("");

		//TextBox del form
		txtNomeUtente = new TextBox();
		txtPassword = new PasswordTextBox();

		//Inserimento widget nella griglia
		grid.setWidget(0, 0, lblNomeUtente);
		grid.setWidget(0, 1, txtNomeUtente);
		grid.setWidget(1, 0, lblPassword);
		grid.setWidget(1, 1, txtPassword);
		grid.setWidget(2, 0, lblWelcome);
		grid.setWidget(2, 1, lblWelcomeU);

		//Bottone login
		Button btnLogin = new Button("Accedi", btnLoginHandler);

		//Popolamento del main panel
		mainPanel.setWidth("100%");
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		mainPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		mainPanel.add(title);
		mainPanel.add(grid);
		mainPanel.add(btnLogin);
	}

	/**
	 * Metodo che si occupa della creazione del ClickHandler del login button
	 */
	private void creazioneHandlerPulsanti() {
		//handler button login
		btnLoginHandler = new ClickHandler() {
			//Gestione evento onClick
			public void onClick(ClickEvent e) {
				String nomeUtente = txtNomeUtente.getText().trim();
				String password = txtPassword.getText();

				//Se nome utente nullo, mando messaggio d'errore
				if (nomeUtente == null || nomeUtente.isEmpty()) {
					Window.alert("Il campo Nome Utente e' obbligatorio \n");
					return;
				}
				//Se password nulla, mando messaggio d'errore
				if (password == null || password.isEmpty()) {
					Window.alert("Il campo password e' obbligatorio \n");
					return;
				}

				//Richiedo al server di effettuare il login
				greetingService.effettuaLogin(nomeUtente, password, new AsyncCallback<String>() {

					//Caso di fallimento
					@Override
					public void onFailure(Throwable caught) {
						//Messaggio d'errore
						Window.alert("c'e' stato un errore" + caught.getMessage());
						lblWelcome.setText("");
						lblWelcomeU.setText("");
					}

					//Caso di successo
					@Override
					public void onSuccess(String result) {
						Window.alert(result);
						if(!result.equalsIgnoreCase("Login errato")) {
							Se1819.utenteConnesso = txtNomeUtente.getText().trim();
							Window.alert("Benvenuto, " + Se1819.utenteConnesso + "!");
							//Redirect alla home
							mainPanel.clear();
							Home h = new Home();
							h.onModuleLoad(mainPanel);
						} else {
							lblWelcome.setText("");
							lblWelcomeU.setText("");
						}
					}
				});

			}
		};

	}
}
