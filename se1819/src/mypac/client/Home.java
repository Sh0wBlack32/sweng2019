package mypac.client;

import java.util.Arrays;
import java.util.Comparator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import mypac.shared.Categoria;
import mypac.shared.Domanda;
import mypac.shared.MyUtil;

public class Home {
	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	private VerticalPanel mainPanel;
	private ListBox listBox;
	private Grid formGrid;
	private Domanda[] listaDomande;
	private Categoria[] listaCategorie;

	private HandlerRegistration hr;
	private Button btnFiltra;


	/**
	 * Metodo che permette il popolamento del pannello principale
	 * @param panel 
	 */
	public void onModuleLoad(VerticalPanel panel) {

		mainPanel = panel;

		//Chiedo al server se l'utente è giudice
		greetingService.isGiudice(Se1819.utenteConnesso, new AsyncCallback<Boolean>() {

			//Caso di fallimento
			@Override
			public void onFailure(Throwable caught) {
				//Messaggio d'errore
				Window.alert("C'è stato un errore nel controllo giudice."
						+ "\nErrore: " + caught.getMessage());
			}
			//Caso di successo
			@Override
			public void onSuccess(Boolean result) {
				//Window.alert("Ritorna: "+result);
				if(result)
					Se1819.isGiudice = result;
				else
					Se1819.isGiudice = result;
				//Window.alert("isGiudice: "+Se1819.isGiudice);
			}
		});
		//Chiedo al server se si tratta di un utente
		greetingService.isUtente(Se1819.utenteConnesso, new AsyncCallback<Boolean>() {
			//Caso di fallimento
			@Override
			public void onFailure(Throwable caught) {
				//Messaggio d'errore
				Window.alert("C'è stato un errore nel controllo giudice."
						+ "\nErrore: " + caught.getMessage());
			}
			//Caso di successo
			@Override
			public void onSuccess(Boolean result) {
				if(result) {
					Se1819.isUtente = true;
				} else {
					Se1819.isUtente = false;
				}
			}
		});

		//Richiedo al server di restituirmi le domande
		greetingService.restituisciDomande(new AsyncCallback<Domanda[]>() {
			//Caso di successo
			@Override
			public void onSuccess(Domanda[] domande) {
				HTML titolo = new HTML("<h1> Domande degli utenti: </h1>");

				//Ordinamento domande
				Arrays.sort(domande, new Comparator<Domanda>() {
					@Override
					public int compare(Domanda d1, Domanda d2) {
						return - d1.getTimestamp().compareTo(d2.getTimestamp());
					}
				});

				listaDomande = domande;

				disegnaGrigliaDomande(domande);

				mainPanel.add(titolo);
				if (Se1819.utenteConnesso!= null && !Se1819.utenteConnesso.isEmpty())
					visualizzaFiltroCategorie();

				mainPanel.add(formGrid);
			}
			//Caso di fallimento
			@Override
			public void onFailure(Throwable caught) {
				//Messaggio d'errore
				Window.alert("C'è stato un errore nella visualizzazione delle domande!"
						+ "\nError: "+ caught.getMessage());
			}
		});
	}

	/**
	 * Metodo per disegnare la griglia delle domande
	 * @param domande le domande da inserire nella griglia
	 */
	private void disegnaGrigliaDomande(Domanda[] domande){
		//se la griglia non e' mai stata istanziata, ne viene creata una
		if (formGrid == null) formGrid = new Grid(domande.length+1, 6);
		//se la griglia esisteva gia', viene ridimensionata
		else formGrid.resizeRows(domande.length+1);

		formGrid.setCellSpacing(10);
		//Titoli
		String[] titoli = {
				"Testo domanda",
				"Autore",
				"Data"
		};
		//Gestione grid
		for (int i = 0; i< titoli.length; i++)
			formGrid.setWidget(0, i, new HTML("<h4 align = 'center'>" + titoli[i] + "</h4>" ));


		for (int i=1; i<=domande.length; i++) {
			Domanda d = domande[i-1];
			Label lblTesto = new Label(d.getTesto());
			Label lblAutore = new Label(d.getUtente().getNomeUtente());
			Label lblData = new Label(MyUtil.formattaTimestamp(d.getTimestamp()));
			Button btn = new Button("→");
			btn.addClickHandler(new VisualizzaRisposteHandler(d));
			Button btnR = new Button("Rispondi");
			btnR.addClickHandler(new RispondiHandler(d));
			Button btn2 = new Button("x");
			btn2.setVisible(false);
			if(Se1819.utenteConnesso.equalsIgnoreCase("admin"))
				btn2.setVisible(true);
			btn2.addClickHandler(new EliminaDomandaHandler(d));

			formGrid.setWidget(i, 0, lblTesto);
			formGrid.setWidget(i, 1, lblAutore);
			formGrid.setWidget(i, 2, lblData);
			formGrid.setWidget(i, 3, btn);
			formGrid.setWidget(i, 4, btnR);
			formGrid.setWidget(i, 5, btn2);
		}

		mainPanel.add(formGrid);

	}

	/**
	 * Metodo per visualizzare il filtro delle categorie
	 */
	private void visualizzaFiltroCategorie(){

		listBox = new ListBox();

		btnFiltra = new Button("Applica Filtri");

		//Richiedo al server di restituirmi le categorie
		greetingService.restituisciCategorie(new AsyncCallback<Categoria[]>() {

			//Caso di fallimento
			@Override
			public void onFailure(Throwable caught) {
				//Messaggio d'errore
				Window.alert("Si e' verificato un errore. \nError: " + caught.getMessage());
			}
			//Caso di successo
			@Override
			public void onSuccess(Categoria[] result) {
				listaCategorie = Arrays.stream(result)
						.filter(c -> c.getPadre() == null).toArray(Categoria[]::new); 
				aggiornaListaEGriglia(listaCategorie, listaDomande);
			}
		});

		Grid grigliaFiltri = new Grid(1,3);
		grigliaFiltri.setWidget(0, 0, new Label("Seleziona categorie"));
		grigliaFiltri.setWidget(0, 1, listBox);
		grigliaFiltri.setWidget(0, 2, btnFiltra);
		mainPanel.add(grigliaFiltri);
	}

	/**
	 * Clickhandler per eliminare la domanda
	 *
	 */
	private class EliminaDomandaHandler implements ClickHandler {
		Domanda dom;

		/**
		 * Costruttore
		 * @param dom
		 */
		public EliminaDomandaHandler(Domanda dom) {
			this.dom = dom;
		}

		//Gestione evento onClick
		@Override
		public void onClick(ClickEvent event) {
			//Richiedo al server di eliminare la domanda
			greetingService.eliminaDomanda(dom, new AsyncCallback<Void>() {
				//Caso di fallimento
				@Override
				public void onFailure(Throwable caught) {
					//Messaggio d'errore
					Window.alert("c'e' stato un errore" + caught.getMessage());

				}
				//Caso di successo
				@Override
				public void onSuccess(Void result) {
					Window.alert("Domanda eliminata con successo!");
					//Una volta eliminata la domanda ricarico la home aggiornandola
					mainPanel.clear();
					Home home = new Home();
					home.onModuleLoad(mainPanel);

				}
			});
		}
	}

	/**
	 * ClickHandler che presenta un campo domanda, che indica
	 * la domanda della quale verranno visualizzate le risposte
	 * quando si clicca sul pulsante a cui viene associato tale handler
	 */
	private class VisualizzaRisposteHandler implements ClickHandler {

		Domanda dom;

		/**
		 * Costruttore
		 * @param dom
		 */
		public VisualizzaRisposteHandler(Domanda dom) {
			this.dom = dom;
		}

		//Gestione evento onClick
		@Override
		public void onClick(ClickEvent event) {
			//Se non ci sono risposte mostro messaggio
			if (dom.getRisposte() == null || dom.getRisposte().isEmpty()) {
				Window.alert("Non ci sono risposte a tale domanda");
			}
			else {
				//Se ci sono risposte le mostro
				mainPanel.clear();
				VisualizzaRisposte vr = new VisualizzaRisposte();
				vr.onModuleLoad(mainPanel, dom);
			}
		}
	}


	/**
	 * ClickHandler che presenta un campo domanda, che indica
	 * la domanda alla quale si vuole rispondere
	 * quando si clicca sul pulsante a cui viene associato tale handler
	 */
	public class RispondiHandler implements ClickHandler {

		Domanda dom;

		/**
		 * Costruttore
		 * @param dom
		 */
		public RispondiHandler(Domanda dom) {
			this.dom = dom;
		}

		//Gestione evento onClick
		@Override
		public void onClick(ClickEvent event) {
			//Se la domanda on è chiusa si può rispondere

			mainPanel.clear();
			InserisciRisposta ir = new InserisciRisposta();
			ir.onModuleLoad(mainPanel, dom);
		}
	}


	/**
	 * Classe che rappresenta il gestore del pulsante "Applica Filtri"
	 */
	private class FilterHandler implements ClickHandler {
		Domanda[] domande;
		Categoria[] categorie;

		/**
		 * Costruttore 
		 * @param domande
		 * @param categorie
		 */
		public FilterHandler(Domanda[] domande, Categoria[] categorie) {
			this.domande = domande;
			this.categorie = categorie;
		}

		//Gestione evento onClick
		@Override
		public void onClick(ClickEvent event) {
			int index = listBox.getSelectedIndex();
			if (index == 0) {
				disegnaGrigliaDomande(listaDomande);
				aggiornaListaEGriglia(listaCategorie, listaDomande);
			}
			else {
				Categoria c = categorie[index-1];
				domande = Arrays.stream(domande)
						.filter(d -> d.getCategoria().contenutaIn(c))
						.toArray(Domanda[]::new);
				if (domande == null || domande.length == 0)
					Window.alert("Non ci sono domande per la categoria selezionata");
				else {
					Categoria [] cs = c.getFigli().toArray(new Categoria[0]);
					aggiornaListaEGriglia(cs, domande);
				}
			}
		}
	}

	/**
	 * Metodo che aggiorna la listBox contenente le categorie da mostrare
	 * e la griglia contenente le domande relative alla categoria selezionata
	 * @param categorie
	 * @param domande
	 */
	private void aggiornaListaEGriglia(Categoria[] categorie, Domanda[] domande) {
		listBox.clear();
		listBox.addItem("-- Azzera Filtri --");
		for (Categoria c : categorie)
			listBox.addItem(c.getNomeCategoria());
		disegnaGrigliaDomande(domande);
		if (hr!= null)
			hr.removeHandler();
		hr = btnFiltra.addClickHandler(new FilterHandler(domande, categorie));
	}
}
