package mypac.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/***
 * 
 * Classe che implementa la schermata per la promozione da utente a giudice da parte dell'admin
 * 
 */
public class NominaGiudice {
	private ClickHandler btnHandler;
	private ListBox listBox;
	private VerticalPanel mainPanel;

	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	/**
	 * Popolamento del pannello principale
	 * @param panel
	 */
	public void onModuleLoad(VerticalPanel panel) {
		creazioneHandlerPulsanti();

		//Se l'utente loggato non e' admin
		if(!Se1819.utenteConnesso.equalsIgnoreCase("admin")) {
			//Messaggio d'errore
			Window.alert("Non si dispone dei permessi necessari");
			//Redirect alla home
			Home h = new Home();
			h.onModuleLoad(panel);
			return;
		}
		//Panel
		mainPanel = panel;
		//Listbox
		listBox = new ListBox();

		//Popolamento list box con il nome degli utenti registrati
		greetingService.restituisciNomiUtentiNonGiudici(new AsyncCallback<String[]>() {
			//Caso di fallimento
			@Override
			public void onFailure(Throwable caught) {
				//Messaggio d'errore
				Window.alert("C'e' stato un errore"
						+ "\nErrore" + caught.getMessage());
			}
			//Caso di successo
			@Override
			public void onSuccess(String[] result) {
				//Se non ci sono utenti
				if (result.length == 0) {
					//Messaggio d'errore
					Window.alert("Non ci sono utenti da promuovere");

					//Ricaricamento della pagina a home
					Home h = new Home();
					mainPanel.clear();
					h.onModuleLoad(panel);
					return;
				}
				
				//Popolamento listbox con nomi utenti
				for (String s: result)
					listBox.addItem(s);
			}
		});

		//Popolamento griglia con la list box
		Grid grid = new Grid(1,2);
		grid.setWidget(0, 0, new Label("Seleziona l'utente"));
		grid.setWidget(0, 1, listBox);

		//Titolo
		HTML title = new HTML("<h1>Nomina giudice un utente semplice:</h1>");

		//Bottone "nomina giudice"
		Button btn = new Button("Nomina giudice", btnHandler);

		//Popolamento del main panel
		mainPanel.setWidth("100%");
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		mainPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		mainPanel.add(title);
		mainPanel.add(grid);
		mainPanel.add(btn);
	}

	/**
	 * Metodo che crea gli handler dei pulsanti
	 */
	private void creazioneHandlerPulsanti() {
		//Handler del bottone nomina giudice
		btnHandler = new ClickHandler() {
			//Gestione evento onClick
			@Override
			public void onClick(ClickEvent e) {
				String nomeUtente = listBox.getSelectedItemText();
				//Richiedo al server di nominare giudice
				greetingService.nominaGiudice(nomeUtente, new AsyncCallback<String>() {
					//Caso di fallimento
					@Override
					public void onFailure(Throwable caught) {
						//Messaggio d'errore
						Window.alert("C'e' stato un errore" + caught.getMessage());
					}
					//Caso di successo
					@Override
					public void onSuccess(String result) {
						Window.alert(result);
						//Ricaricamento della pagina
						NominaGiudice ng = new NominaGiudice();
						mainPanel.clear();
						ng.onModuleLoad(mainPanel);
					}
				});
			}
		};
	}
}
