package mypac.client;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe che rappresenta la barra di navigazione
 *
 */
public class MyMenuBar {
	VerticalPanel mainPanel;
	MenuBar myMenu;
	
	/**
	 * Costruttore
	 * @param panel pannello principale
	 */
	public MyMenuBar(VerticalPanel panel) {
		this.myMenu = new MenuBar();
		this.mainPanel = panel;
		myMenu.setPixelSize(RootPanel.get().getOffsetWidth(), 25);
	}

	/**
	 * Metodo che aggiunge alla barra un menu item
	 * che permette di passare alla schermata per
	 * l'inserimento dei dati per la registrazione
	 */
	public void addNavRegistrazione() {
		Command cmdRegistrazione = new Command() {
			@Override
			public void execute() {
				//caricamento della schermata della registrazione
				mainPanel.clear();
				Registrazione r = new Registrazione();
				r.onModuleLoad(mainPanel);
			}
		};
		//aggiunta del menu item alla barra di navigazione
		myMenu.addItem(new MenuItem("Registrati", cmdRegistrazione));
	}

	/**
	 * Metodo che aggiunge alla barra un menu item
	 * che permette di passare alla schermata principale
	 */
	public void addNavHome() {
		Command cmdHome = new Command() {
			@Override
			public void execute() {
				//caricamento della schermata principale
				mainPanel.clear();
				Home home = new Home();
				home.onModuleLoad(mainPanel);
			}
		};
		//aggiunta del menu item alla barra di navigazione
		myMenu.addItem(new MenuItem("Home", cmdHome));
	}

	/**
	 * Metodo che aggiunge alla barra un menu item
	 * che permette di passare alla schermata del login
	 */
	public void addNavLogin() {
		Command cmdLogin = new Command() {
			@Override
			public void execute() {
				//caricamento della schermata del login
				mainPanel.clear();
				Login l = new Login();
				l.onModuleLoad(mainPanel);
			}
		};
		//aggiunta del menu item alla barra di navigazione
		myMenu.addItem(new MenuItem("Login", cmdLogin));
	}

	/**
	 * Metodo che aggiunge alla barra un menu item
	 * che permette di effettuare il logout
	 */
	public void addNavLogout() {
		Command cmdLogout = new Command() {
			@Override
			public void execute() {
				//controllo se l'utente e' loggato o meno
				if (Se1819.utenteConnesso.isEmpty() || Se1819.utenteConnesso == null || Se1819.utenteConnesso.equalsIgnoreCase(""))
					Window.alert("Nessun utente loggato, impossibile effettuare il logout");
				else {
					//se loggato, effettuo il logout
					Se1819.utenteConnesso = "";
					Window.alert("Logout effettuato");
				}
				//caricamento della schermata principale
				mainPanel.clear();
				Home home = new Home();
				home.onModuleLoad(mainPanel);
			}
		};
		//aggiunta del menu item alla barra di navigazione
		myMenu.addItem(new MenuItem("Logout", cmdLogout));
	}

	/**
	 * Metodo che aggiunge alla barra un menu item
	 * che permette di passare alla schermata per la 
	 * gestione delle categorie
	 */
	public void addNavCategoria() {
		Command cmdGestioneCategoria = new Command() {
			@Override
			public void execute() {
				//caricamento della schermata per la gestione della categorie
				mainPanel.clear();
				GestioneCategoria gc = new GestioneCategoria();
				gc.onModuleLoad(mainPanel);
			}
		};
		//aggiunta del menu item alla barra di navigazione
		myMenu.addItem(new MenuItem ("Gestione categorie", cmdGestioneCategoria));
	}

	/**
	 * Metodo che ritorna la barra dei menu
	 * @return la barra dei menu
	 */
	public MenuBar getMenuBar() {
		return myMenu;
	}

	/**
	 * Metodo che aggiunge alla barra un menu item
	 * che permette di passare alla schermata per 
	 * l'inserimento di una nuova domanda
	 */
	public void addNavNuovaDom() {
		Command cmdNuovaDom = new Command() {
			@Override
			public void execute() {
				//caricamento della schermata per l'inserimento della domanda
				mainPanel.clear();
				InserisciDomanda insDom = new InserisciDomanda();
				insDom.onModuleLoad(mainPanel);
			}
		};
		//aggiunta del menu item alla barra di navigazione
		myMenu.addItem(new MenuItem ("Nuova domanda", cmdNuovaDom));
	}

	/**
	 * Metodo che aggiunge alla barra un menu item
	 * che permette di passare alla schermata per la
	 * nomina degli utenti giudici
	 */
	public void addNavNominaGiudice() {
		Command cmdNominaGiudice = new Command() {
			@Override
			public void execute() {
				//caricamento della schermata per la nomina dei giudici
				mainPanel.clear();
				NominaGiudice nomGiu = new NominaGiudice();
				nomGiu.onModuleLoad(mainPanel);
			}
		};
		//aggiunta del menu item alla barra di navigazione
		myMenu.addItem(new MenuItem ("Nomina giudice", cmdNominaGiudice));
	}
	
	/**
	 * Metodo che aggiunge alla barra un menu item
	 * che permette di passare alla schermata per
	 * la visualizzazione del proprio profilo
	 */
	public void addNavProfilo() {
		Command cmdProfilo = new Command() {
			@Override
			public void execute() {
				//caricamento della schermata per la visualizzazione del profilo
				mainPanel.clear();
				Profilo p = new Profilo();
				p.onModuleLoad(mainPanel);				
			}
		};
		//aggiunta del menu item alla barra di navigazione
		myMenu.addItem(new MenuItem("Profilo", cmdProfilo));
	}
}
