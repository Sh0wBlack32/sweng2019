package mypac.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import mypac.shared.Categoria;

/**
 * Classe che permette all'amministratore di rinominare una categoria esistente oppure di aggiungerne una nuov
 *
 */
public class GestioneCategoria {
	
	private VerticalPanel mainPanel;
	private Button aggiungiCategoria; 
	private Categoria[] categorie;
	private Grid griglia;
	
	//Si richiama il server
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	
	/**
	 * Creazione dell'interfaccia mediante il popolamento del pannello principale
	 * @param panel
	 */
	public void onModuleLoad(VerticalPanel panel) {
		mainPanel = panel;
		
		//Se l'utente loggato non e' amministratore, questa schermata non verr� mostrata
		if(!Se1819.utenteConnesso.equalsIgnoreCase("admin")) {
			//Messaggio d'errore
			Window.alert("Non si dispone dei permessi necessari");
			Home h = new Home();
			//Redirect alla schermata di home
			h.onModuleLoad(panel);
			return;
		}
		
		//Richiedo al server di restituire la lista delle categorie
		greetingService.restituisciCategorie(new AsyncCallback<Categoria[]>() {

			//In caso di failure
			@Override
			public void onFailure(Throwable caught) {
				//Messaggio d'errore
				Window.alert("Non e' possibile recuperare le categorie" + caught.getMessage());
			}

			//In caso di successo restituisce i risultati
			@Override
			public void onSuccess(Categoria[] result) {
				categorie = result;
				
				//Aggiunta titolo al panel
				mainPanel.add(new HTML("<h1>Gestione categorie</h1>"));
				//Creazione button per aggiungere categorie
				aggiungiCategoria = new Button("Aggiungi Categoria");
				//Clickhandler aggiungi categorie
				aggiungiCategoria.addClickHandler(new BtnAggiungiCategoriaHandler());
				
				//Creazione griglia
				griglia = new Grid(categorie.length, 2);
				//Popolamento griglia con il nome delle categorie
				for (int r=0; r<categorie.length; r++) {
					griglia.setWidget(r, 0, new Label(categorie[r].getNomeCategoria()));
					griglia.setWidget(r, 1, new Button("Rinomina", new BtnRinominaHandler(categorie[r])));
				}
				
				//Aggiunta della griglia al panel
				mainPanel.add(aggiungiCategoria);
				mainPanel.add(griglia);	
			}
		});
	}
	
	/**
	 * Classe che rappresenta il gestore del pulsante "Rinomina" 
	 */
	private class BtnRinominaHandler implements ClickHandler {
		Categoria c;
		
		/**
		 * Costruttore
		 * @param c categoria da rinominare
		 */
		public BtnRinominaHandler(Categoria c) {
			this.c = c;
		}

		//Gestione evento onClick
		@Override
		public void onClick(ClickEvent event) {
			//Creazione panel
			VerticalPanel vp = new VerticalPanel();
			vp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			vp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
			vp.setSpacing(2);
			//Creazione dialogBox
			DialogBox dialog = new DialogBox();
			//Creazione textBox
			TextBox txtCategoria = new TextBox();
			txtCategoria.setText(c.getNomeCategoria());
			//Aggiunta label, textBox e buttons
			vp.add(new Label("Inserisci il nuovo nome della categoria"));
			vp.add(txtCategoria);
			vp.add(new Button("OK", new BtnConfermaRinominaHandler(c, txtCategoria, dialog)));
			vp.add(new Button("Annulla", new ClickHandler() {
				//Gestione onClick
				@Override
				public void onClick(ClickEvent event) {
					//Nascondi finestra di dialogo
					dialog.hide();
				}
			}));
			dialog.setText("Rinomina categoria");
			dialog.center();
			dialog.add(vp);
			dialog.show();	
		}	
	}
	
	/**
	 * Classe che rappresenta il gestore del pulsante "Aggiungi categoria"
	 */
	private class BtnAggiungiCategoriaHandler implements ClickHandler {
		
		//Gestione evento onClick
		@Override
		public void onClick(ClickEvent event) {
			//Creazione dialogBox
			DialogBox dialog = new DialogBox();
			//Creazione verticalPanel
			VerticalPanel vp = new VerticalPanel();
			vp.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			vp.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
			vp.setSpacing(2);
				
			//Creazione listBox
			ListBox elencoCategorie = new ListBox();
			//Disabilito elenco categorie
			elencoCategorie.setEnabled(false);
			//Popolo con il nome delle categorie
			for (Categoria c: categorie)
				elencoCategorie.addItem(c.getNomeCategoria());
				
			//Creazione textBox
			TextBox txtNome = new TextBox();
			//listbox che consente di scegliere se creare una categoria o una sottocategoria
			ListBox listBox = new ListBox();
			listBox.addItem("Categoria");
			listBox.addItem("Sottocategoria");
			//Clickhandler della listBox
			listBox.addClickHandler(new ClickHandler() {
				//Gestione evento onClick
				@Override
				public void onClick(ClickEvent event) {					
					if (listBox.getSelectedIndex() == 0)
						elencoCategorie.setEnabled(false);
					else 
						elencoCategorie.setEnabled(true);					
				}
			});
			//Aggiunta label
			vp.add(new Label("Inserisci il nome della categoria"));		
			vp.add(txtNome);
			//Aggiunta label
			vp.add(new Label("Scegli la tipologia"));
			//Aggiunta lixtBox
			vp.add(listBox);
			vp.add(elencoCategorie);
			//Aggiunta buttons
			vp.add(new Button("OK", new BtnConfermaInserimentoHandler(txtNome, elencoCategorie, dialog)));
			//Clickhandler bottone "annulla"
			vp.add(new Button("Annulla", new ClickHandler() {
				//Gestione evento onClick
				@Override
				public void onClick(ClickEvent event) {
					//Nascondo finestra di dialogo
					dialog.hide();
				}
			}));
			dialog.add(vp);
			dialog.setText("Aggiunta categoria");
			dialog.center();
			dialog.show();
		}
	}
	
	/**
	 * Classe che rappresenta il gestore del pulsante "Ok"
	 * per la confermare l'inserimento della nuova categoria
	 */
	private class BtnConfermaInserimentoHandler implements ClickHandler {
		TextBox txtNome;
		ListBox elencoCategorie;
		DialogBox dialog;
		
		/**
		 * Costruttore
		 * @param txtNome 
		 * @param elencoCategorie
		 * @param dialog 
		 */
		public BtnConfermaInserimentoHandler(TextBox txtNome, ListBox elencoCategorie, DialogBox dialog ) {
			this.txtNome = txtNome;
			this.elencoCategorie = elencoCategorie;
			this.dialog = dialog;
		}
		
		//Gestione evento onClick
		@Override
		public void onClick(ClickEvent event) {
			String nomeCategoria = txtNome.getText().trim();
			//Se nome categoria vuoto, messaggio d'errore
			if (nomeCategoria == null || nomeCategoria.isEmpty()) {
				Window.alert("Il nome della categoria non pu� essere vuoto");
				return;
			}
			//Richiedo al server di inserire una categoria
			greetingService.inserisciCategoria(nomeCategoria, new AsyncCallback<Integer>() {
				//Caso di fallimento
				@Override
				public void onFailure(Throwable caught) {
					//Messaggio d'errore
					Window.alert("Errore nell'aggiunta della categoria \n" + caught.getMessage());
				}
				//Caso di successo
				@Override
				public void onSuccess(Integer result) {
					// se la nuova categoria NON e' una sottocategoria
					if (!elencoCategorie.isEnabled()) {
						Window.alert("Categoria " + nomeCategoria + " aggiunta con successo");
						ricaricaMainPanel();		
					}
					//se la categoria e' una sottocategoria
					else {
						int index = elencoCategorie.getSelectedIndex();
						Categoria padre = categorie[index];
						int idPadre = padre.getId();
						int idFiglio = result;
						//Richiedo al server di inserire una sotto-categoria
						greetingService.inserisciSottocategoria(idPadre, idFiglio, new AsyncCallback<Void>() {
							//Caso di fallimento
							@Override
							public void onFailure(Throwable caught) {
								//Messaggio d'errore
								Window.alert("Errore nell'aggiunta della sottocategoria \n " 
										+ caught.getMessage());
							}
							//Caso di successo
							@Override
							public void onSuccess(Void result) {
								Window.alert(nomeCategoria + 
										" aggiunta con successo alla categoria " + padre.getNomeCategoria());
								ricaricaMainPanel();
							}
						});
					}
				}
			});
			dialog.hide();
		}
	}
	
	
	/**
	 * Classe per la gestione del pulsante "Ok" utilizzato
	 * per confermare il nuovo nome della categoria
	 */
	private class BtnConfermaRinominaHandler implements ClickHandler {
		Categoria c;
		TextBox txtBox;
		DialogBox dialog;
		
		/**
		 * Costruttore 
		 * @param c
		 * @param txtBox
		 * @param dialog
		 */
		public BtnConfermaRinominaHandler(Categoria c, TextBox txtBox, DialogBox dialog) {
			this.c = c;
			this.txtBox = txtBox;
			this.dialog = dialog;
		}
		
		//Gestione evento onClick
		@Override
		public void onClick(ClickEvent event) {
			String nuovoNome = txtBox.getText().trim();
			//Richiedo al server di rinominare la categoria
			greetingService.rinominaCategoria(c, nuovoNome, new AsyncCallback<Void>() {
				//Caso di fallimento
				@Override
				public void onFailure(Throwable caught) {
					//Messaggio d'errore
					Window.alert("Errore nell'aggiornamento della categoria. \n" + caught.getMessage());
				}
				//Caso di successo
				@Override
				public void onSuccess(Void result) {
					dialog.hide();
					//Messaggio di conferma
					Window.alert("Categoria aggiornata correttamente");
					ricaricaMainPanel();					
				}
			});
		}
	}	
	
	/**
	 * Metodo che pulisce il pannello principale e 
	 * lo ricarica con i nuovi dati
	 */
	public void ricaricaMainPanel() {
		mainPanel.clear();
		GestioneCategoria gc = new GestioneCategoria();
		gc.onModuleLoad(mainPanel);		
	}
}
