package mypac.client;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import mypac.shared.Categoria;
import mypac.shared.Domanda;
import mypac.shared.Giudizio;
import mypac.shared.MyUtil;
import mypac.shared.Risposta;
import mypac.client.Se1819;

/**
 * Classe che rappresenta la schermata che permette la 
 * visualizzazione delle risposte associate ad una
 * specifica domanda
 *
 */
public class VisualizzaRisposte {
	private Grid grid;
	private Label lblPath = new Label();
	private VerticalPanel mainPanel;
	private Domanda domanda;

	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	String votoTemp = "";

	/**
	 * Metodo che permette la creazione dell'interfaccia grafica
	 * per la visualizzazione delle risposte associate ad una 
	 * specifica domanda
	 * @param panel il pannello principale
	 * @param dom la domanda di cui si visualizzano le risposte
	 */
	public void onModuleLoad(VerticalPanel panel, Domanda dom) {
		mainPanel = panel;
		domanda = dom;
		Risposta[] risp = domanda.getRisposte().toArray(new Risposta[0]);
		int numRisp = risp.length;
		grid = new Grid(numRisp+1,8);

		//selezione criterio ordinamento in base alla tipologia dell'utenza
		if (Se1819.utenteConnesso == null || Se1819.utenteConnesso.isEmpty())
			ordinamentoCronologico(risp);
		else
			ordinamentoInBaseAiGiudizi(risp);

		//intestazione griglia
		grid.setWidget(0, 0, new HTML("<h4 align = 'center'>Giudizio medio</h4>"));
		grid.setWidget(0, 1, new HTML("<h4 align = 'center'>Testo della risposta</h4>"));
		grid.setWidget(0, 3, new HTML("<h4 align = 'center'>Autore</h4>"));
		grid.setWidget(0, 2, new HTML("<h4 align = 'center'>Data e ora</h4>"));
		grid.setWidget(0, 7, new HTML("<h4 align = 'center'>Lista giudizi</h4>"));
		
		//aggiunta delle risposte ordinate alla griglia
		for (int i = 0; i < numRisp; i++) {
			Label lblVoto = new Label(risp[i].getVoto());
			Label lblTesto = new Label(risp[i].getTesto());
			Label lblTimestamp = new Label(MyUtil.formattaTimestamp(risp[i].getTimestamp()));
			Label lblAutore = new Label(risp[i].getNomeUtente());
			ListBox listGiudizio = new ListBox();
			listGiudizio.setVisible(false);
			Button btnGiudizioExe = new Button("Giudica");
			btnGiudizioExe.setVisible(false);
			Button btnCancRisp = new Button("x");
			btnCancRisp.setVisible(false);
			if(Se1819.isGiudice) {
				btnCancRisp.setVisible(true);
				btnGiudizioExe.setVisible(true);
				listGiudizio.setVisible(true);
			}
			else if(Se1819.utenteConnesso.equalsIgnoreCase("admin")) {
				btnCancRisp.setVisible(true);
			}
			//l'indice delle righe e' diverso perche' nella riga 0 c'e' l'intestazione
			int r = i+1;

			//inserimento possibili voti nella listbox
			listGiudizio.addItem("");
			listGiudizio.addItem("*");
			listGiudizio.addItem("**");
			listGiudizio.addItem("***");
			listGiudizio.addItem("****");
			listGiudizio.addItem("*****");
			
			//inserimento coppie (utente, voto) che hanno giudicato la domanda
			List<Giudizio> listaVoti = risp[i].getListaGiudizi();
			int size = (listaVoti!= null ? listaVoti.size() : 0);
			Grid gridGiudizi = new Grid(size , 2);
			for (int j=0; j<size; j++) {
				Giudizio g = listaVoti.get(j);
				gridGiudizi.setWidget(j, 0, new Label(g.getGiudice().getNomeUtente()));
				gridGiudizi.setWidget(j, 1, new Label(g.getVoto()));
			}
			
			//true o false se la risposta presenta un giuzizio espresso da questo utente
			boolean giaVotato = risp[i].getListaGiudizi().stream()
					.anyMatch(g -> g.getGiudice().getNomeUtente().equals(Se1819.utenteConnesso));
			//se ha gia votato, disattivo la listbox e il pulsante
			if (giaVotato) {
				listGiudizio.setEnabled(false);
				btnGiudizioExe.setEnabled(false);
			}
			else 
				btnGiudizioExe.addClickHandler(new GiudicaClickHandler(risp[i], listGiudizio));
			
			btnCancRisp.addClickHandler(new MyClickHandlerCanc(risp[i]));
			//aggiunta degli elementi alla griglia
			grid.setWidget(r, 0, lblVoto);
			grid.setWidget(r, 1, lblTesto);
			grid.setWidget(r, 2, lblTimestamp);
			grid.setWidget(r, 3, lblAutore);
			grid.setWidget(r, 4, listGiudizio);
			grid.setWidget(r, 5, btnGiudizioExe);
			grid.setWidget(r, 6, btnCancRisp);
			grid.setWidget(r, 7, gridGiudizi);
		}

		//aggiunta degli elementi all'interfaccia grafica
		HTML titolo = new HTML("<h1> Risposte degli utenti: </h1>");
		mainPanel.add(titolo);
		creazionePathCategoria(domanda);
		mainPanel.add(lblPath);
		//griglia che contiene le info sulla domanda
		Grid gridDomanda = new Grid(2, 3);
		gridDomanda.setCellPadding(5);
		gridDomanda.setWidget(0, 0, new HTML("<h4 align = 'center'>Testo della domanda</h4>"));
		gridDomanda.setWidget(0, 1, new HTML("<h4 align = 'center'>Autore</h4>"));
		gridDomanda.setWidget(0, 2, new HTML("<h4 align = 'center'>Data</h4>"));
		gridDomanda.setWidget(1, 0, new Label(domanda.getTesto()));
		gridDomanda.setWidget(1, 1, new Label(domanda.getUtente().getNomeUtente()));
		gridDomanda.setWidget(1, 2, new Label(MyUtil.formattaTimestamp(domanda.getTimestamp())));
		mainPanel.add(gridDomanda);
		grid.setCellPadding(5);
		mainPanel.add(grid);

	}

	/**
	 * Metodo che consente di ordinare un array di risposte in base
	 * all'ordinamento cronologico, dalla pi� alla meno recente
	 * @param risposte array da ordinare
	 */
	private void ordinamentoCronologico(Risposta[] risposte) {
		Arrays.sort(risposte, new Comparator<Risposta>() {
			@Override
			public int compare(Risposta r1, Risposta r2) {
				return - r1.getTimestamp().compareTo(r2.getTimestamp());
			}
		});
	}

	/**
	 * Metodo che consente di ordinare un array di risposte in base
	 * alla media dei giudizi che ciascuna risposta presenta.
	 * @param risposte array da ordinare
	 */
	private void ordinamentoInBaseAiGiudizi (Risposta[] risposte) {

		Arrays.sort(risposte, new Comparator<Risposta>() {
			@Override
			public int compare(Risposta r1, Risposta r2) {
				List<Giudizio> listaGiudizi1 = r1.getListaGiudizi();
				List<Giudizio> listaGiudizi2 = r2.getListaGiudizi();
				//va prima la risposta che ha giudizi
				if (!listaGiudizi1.isEmpty() && listaGiudizi2.isEmpty())
					return -1;
				else if (listaGiudizi1.isEmpty() && !listaGiudizi2.isEmpty())
					return 1;
				//se entrambe le risposte hanno dei giudizi confronto la media dei giudizi
				else if (!listaGiudizi1.isEmpty() && !listaGiudizi2.isEmpty()) {
					int mediaR1 = r1.getVoto().length();
					int mediaR2 = r2.getVoto().length();
					if (mediaR1 > mediaR2) return -1;
					else if (mediaR1 < mediaR2) return 1;
					else return - r1.getTimestamp().compareTo(r2.getTimestamp());
				}
				//se le risposte non hanno giudizi, va prima la piu recente
				else
					return - r1.getTimestamp().compareTo(r2.getTimestamp());
			}
		});
	}

	/**
	 * Metodo che restituisce la categoria della domanda 
	 * e le sue eventuali sopracategorie
	 * @param d la domanda di cui mostrare la categoria
	 */
	private void creazionePathCategoria(Domanda d) {

		greetingService.restituisciCategorie(new AsyncCallback<Categoria[]>() {
			//caso di insuccesso
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("errore" + caught.getMessage());
			}
			//caso di successo
			@Override
			public void onSuccess(Categoria[] result) {
				Categoria c = d.getCategoria();
				Stack<Categoria> s = new Stack<Categoria>();
				//risale fino al padre
				do {
					s.add(c);
					c = c.getPadre();
				} while (c != null) ;

				String path = "Categoria: " + s.pop().getNomeCategoria();
				while (!s.isEmpty()) {
					path += " >> " + s.pop().getNomeCategoria();
				}
				lblPath.setText(path);
			}
		});
	}

	/**
	 * ClickHandler relativo al pulsante "Giudica".
	 * Permette l'inserimento del voto e di aggiornare le informazioni
	 * presenti nella tabella
	 */
	private class GiudicaClickHandler implements ClickHandler {
		Risposta r;
		ListBox voto;

		/**
		 * Costruttore
		 * @param r la risposta a cui va aggiunto il voto
		 * @param voto la listBox che contiene il voto da inserire
		 */
		public GiudicaClickHandler(Risposta r, ListBox voto) {
			this.r = r;
			this.voto = voto;
		}

		@Override
		public void onClick(ClickEvent event) {
			//controllo sul voto
			if (voto.getSelectedItemText() == null || voto.getSelectedItemText().isEmpty()) {
				Window.alert("Il campo voto e' vuoto \n");
				return;
			}
			//inserimento del voto nel database
			greetingService.inserisciVoto(r, voto.getSelectedItemText().trim(), Se1819.utenteConnesso, new AsyncCallback<String>() {
				//caso di insuccesso
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("C'e' stato un errore. "
							+ "\nErrore: " + caught.getMessage());
				}
				//caso di successo
				@Override
				public void onSuccess(String result) {
					greetingService.restituisciDomanda(domanda, new AsyncCallback<Domanda>() {
						//caso di insuccesso
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("errore" + caught.getMessage());
						}
						//caso di successo
						@Override
						public void onSuccess(Domanda result) {
							//viene aggiornata la domanda e ricaricata la pagina
							domanda = result;
							ricaricaPagina();	
						}
					});
				}
			});
		}
	}

	/**
	 * ClickHandler per l'eliminazione delle risposte
	 */
	private class MyClickHandlerCanc implements ClickHandler {
		Risposta r;
		
		/**
		 * Costruttore
		 * @param r la risposta da eliminare
		 */
		public MyClickHandlerCanc(Risposta r) {
			this.r = r;
		}

		@Override
		public void onClick(ClickEvent event) {
			//eliminazione della risposta dal database
			greetingService.eliminaR(r, domanda, new AsyncCallback<String>() {
				//caso di insuccesso
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("C'e' stato un errore. "
							+ "\nErrore: " + caught.getMessage());
				}
				//caso di successo
				@Override
				public void onSuccess(String result) {
					Window.alert(result);
					//ricaricamento della pagina con la domanda aggiornata
					domanda.removeRisposta(r);
					ricaricaPagina();
				}
			});
		}
	}
	
	/**
	 * Metodo che consente di ricaricare la pagina
	 */
	private void ricaricaPagina() {
		VisualizzaRisposte vr = new VisualizzaRisposte();
		mainPanel.clear();
		vr.onModuleLoad(mainPanel, domanda);
	}
}
