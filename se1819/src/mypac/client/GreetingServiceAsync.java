package mypac.client;

import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import mypac.shared.AccountSocial;
import mypac.shared.Allegato;
import mypac.shared.Categoria;
import mypac.shared.Domanda;
import mypac.shared.Risposta;
import mypac.shared.Utente;
import mypac.shared.UtenteGiudice;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {

	void effettuaRegistrazione(String nomeUtente, String password, String nome, String cognome, String email, char sesso, Date dataNascita,
			String luogoNascita, String domicilio, List<AccountSocial> listaSocial, AsyncCallback<String> callback);

	void restituisciDomande(AsyncCallback<Domanda[]> callback);

	void effettuaLogin(String nomeUtente, String password, AsyncCallback<String> asyncCallback);

	void inserisciRisposta(Domanda d, String testoRisposta, Date timestamp, String nomeUtente,
			AsyncCallback<String> asyncCallback);

	void effettuaDomanda(String testo, Categoria categoria, List<Allegato> allegato, List<Risposta> risposte, Date data,
			String nomeUtente, AsyncCallback<String> callback);


	void restituisciCategorie(AsyncCallback<Categoria[]> callback);

	void rinominaCategoria(Categoria c, String nuovoNome, AsyncCallback<Void> callback);

	void inserisciCategoria(String nomeCategoria, AsyncCallback<Integer> asyncCallback);

	void inserisciSottocategoria(int idPadre, int idFiglio, AsyncCallback<Void> callback);

	void inserisciVoto(Risposta r, String voto, String nomeGiudice, AsyncCallback<String> asyncCallback);

	void leggiVoto(Risposta risposta, AsyncCallback<String> asyncCallback);

	void isGiudice(String utenteConnesso, AsyncCallback<Boolean> asyncCallback);

	//void promuoviGiudice(Utente utente, AsyncCallback<String> callback);

	void eliminaR(Risposta r, Domanda d, AsyncCallback<String> asyncCallback);

	void restituisciUtenti(AsyncCallback<Utente[]> callback);

	void eliminaDomanda(Domanda dom, AsyncCallback<Void> callback);

	void nominaGiudice(String nomeUtente, AsyncCallback<String> callback);

	void getGiudice(String utenteConnesso, AsyncCallback<UtenteGiudice> callback);

	void restituisciUtente(String nomeUtente, AsyncCallback<Utente> callback);

	//void isAdmin(String utenteConnesso, AsyncCallback<Boolean> callback);

	void isUtente(String utenteConnesso, AsyncCallback<Boolean> callback);
	void restituisciNomiUtentiNonGiudici(AsyncCallback<String[]> callback);

	void restituisciDomanda(Domanda domanda, AsyncCallback<Domanda> asyncCallback);

	/*void restituisciRisposte(Domanda domanda, AsyncCallback<List<Risposta>> callback);*/

}
