package mypac.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import mypac.shared.AccountSocial;

/** 
 * Classe che implementa la schermata per effettuare la registrazione alla piattaforma
 * 
 */
public class Registrazione {
	private Label lblNomeUtente, lblPassword, lblEmail, lblNome, lblCognome, 
			lblSesso, lblDataNascita, lblLuogoNascita, lblDomicilio;
	private List<AccountSocial> listaSocial;
	private RadioButton optM, optF;
	private DateBox dpDataNascita;
	private TextBox txtNomeUtente, txtNome, txtCognome, txtEmail, 
			txtLuogoNascita, txtDomicilio;
	private PasswordTextBox txtPassword;
	private Grid formGrid, gridSocial;
	private VerticalPanel mainPanel;
		
	private ClickHandler btnRegisterHandler, btnAddSocialHandler, btnDelSocialHandler;
	
	private DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy");
	
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	
	/**
	 * Creazione dell'interfaccia mediante il popolamento del pannello principale
	 * @param panel
	 */
	@SuppressWarnings("deprecation")
	public void onModuleLoad(VerticalPanel panel) {
		creazioneHandlerPulsanti();
		//Panel
		mainPanel = panel;
		//Grid
		formGrid = new Grid(10, 2);
		
		//Titolo
		HTML title = new HTML("<h1>Registrazione</h1>");
		
		//Etichette del form
		lblNomeUtente = new Label("Nome utente*:");
		lblPassword = new Label("Password*:");
		lblEmail = new Label("Email*:");
		lblNome = new Label("Nome:");
		lblCognome = new Label("Cognome:");
		lblSesso = new Label("Sesso:");
		lblDataNascita = new Label("Data di nascita:");
		lblLuogoNascita = new Label("Luogo di nascita:");
		lblDomicilio = new Label("Luogo di domicilio o di residenza:");
		
		//TextBox del form
		txtNomeUtente= new TextBox();
		txtPassword = new PasswordTextBox();
		txtEmail = new TextBox(); 
		txtNome = new TextBox();
		txtCognome = new TextBox();
		optM = new RadioButton("Sesso", "M");
		optM.setValue(true);
		optF = new RadioButton("Sesso", "F");
		dpDataNascita = new DateBox();
		dpDataNascita.setValue(new Date("01/01/1990"));
		dpDataNascita.setFormat(new DateBox.DefaultFormat(format));
		txtLuogoNascita = new TextBox();
		txtDomicilio = new TextBox();
		
		//griglia che contiene i due radiobutton per la selezione del sesso
		Grid gridSesso = new Grid(1,2);
		gridSesso.setWidget(0, 0, optM);
		gridSesso.setWidget(0, 1, optF);
		
		//Buttons per rimuovere o aggiungere i social
		Button btnAddSocial = new Button("Aggiungi social", btnAddSocialHandler);
		Button btnDelSocial = new Button("Rimuovi social", btnDelSocialHandler);
		
		//Lista widgets
		Widget[] listaWidget = new Widget[] {
				lblNomeUtente, txtNomeUtente, lblPassword, txtPassword, lblEmail, txtEmail,
				lblNome, txtNome, lblCognome, txtCognome, lblSesso, gridSesso, 
				lblDataNascita, dpDataNascita, lblLuogoNascita, txtLuogoNascita,
				lblDomicilio, txtDomicilio, btnAddSocial, btnDelSocial};
		
		//Popolamento della griglia con i widget
		int i = 0;
		for (int r = 0; r<formGrid.getRowCount(); r++)
			for (int c = 0; c<formGrid.getColumnCount(); c++) {
				formGrid.setWidget(r, c, listaWidget[i]);
				i++;
			}

		//Bottone "registrati"
		Button btnRegister = new Button("Registrati!", btnRegisterHandler);
		
		//Griglia che conterre' le informazioni sui social
		gridSocial = new Grid(0, 4);
		
		//Popolamento del main panel
		mainPanel.add(title);
		mainPanel.add(formGrid);
		mainPanel.add(gridSocial);
		mainPanel.add(btnRegister);
	}
	
	/**
	 * Metodo che si occupa della creazione dei ClickHandler dei vari button
	 */
	private void creazioneHandlerPulsanti() {
		//handler button registrazione
		btnRegisterHandler = new ClickHandler() {
			//Gestione evento onClick
			@SuppressWarnings("deprecation")
			public void onClick(ClickEvent e) {
				String nomeUtente = txtNomeUtente.getText().trim();
				String password = txtPassword.getText();
				String email = txtEmail.getText().trim();
							
				//Se nome utente nullo, messaggio d'erroer
				if (nomeUtente == null || nomeUtente.isEmpty()) {
					Window.alert("Il campo Nome Utente e' obbligatorio \n");
					return;
				}
				//Se il nome utente e' admin, messaggio d'errore
				else if (nomeUtente.equalsIgnoreCase("admin")) {
					Window.alert("E' vietato utilizzare il nome \"Admin\" per il nome utente \n");
					return;
				}
				//Se la password e' nulla, messaggio d'errore
				if (password == null || password.isEmpty()) {
					Window.alert("Il campo password e' obbligatorio \n");
					return;
				}
				//Se l'email e' nulla, messaggio d'errore
				if (email == null || email.isEmpty()) {
					Window.alert("Il campo email e' obbligatorio");
					return;
				}
							
				String nome = txtNome.getText().trim();
				String cognome = txtCognome.getText().trim();
				char sesso = optM.getValue()  ?  'M'  : 'F'; 
				
				Date dataNascita = dpDataNascita.getValue();
				//Controllo sulla validita' della data
				Date date = new Date();
				date.setYear(date.getYear() - 18);	
				if (dataNascita.after(date) || dataNascita.before(new Date("01/01/1900"))) {
					Window.alert("Utente non maggiorenne o data non valida.");
					return;
				}
			
				String luogoNascita = txtLuogoNascita.getText().trim();
				String domicilioResidenza = txtDomicilio.getText().trim();
						
				//Se i campi obbligatori non sono nulli, aggiungo gli account social associandoli all'user
				listaSocial = new ArrayList<AccountSocial>();
				for (int i = 0; i < gridSocial.getRowCount(); i++) {
					String username = ((TextBox) gridSocial.getWidget(i, 1)).getText().trim();
					String piattaforma = ((TextBox) gridSocial.getWidget(i, 3)).getText().trim();
					if ((username != null) && (!username.isEmpty()) && 
							(piattaforma != null) && (!piattaforma.isEmpty())) {
						listaSocial.add(new AccountSocial(username, piattaforma));
					}
				}
					
				//Richiedo al server di effettuare la registrazione
				greetingService.effettuaRegistrazione(nomeUtente, password, nome, cognome, email, sesso, dataNascita, 
						luogoNascita, domicilioResidenza, listaSocial, new AsyncCallback<String>() {

					//Caso di fallimento
					@Override
					public void onFailure(Throwable caught) {
						//Messaggio d'errore
						Window.alert("c'e' stato un errore" + caught.getMessage());
					}

					//Caso di successo
					@Override
					public void onSuccess(String result) {
						Window.alert(result);
						if (result.equals("Registrazione effettuata")) {
							//Redirect al login
							mainPanel.clear();
							Login l = new Login();
							l.onModuleLoad(mainPanel);
						}
					}
				});
			}
		};
				
		//Handler button per l'aggiunta di un social
		btnAddSocialHandler = new ClickHandler() {
			//Gestione evento onClick
			public void onClick(ClickEvent e) {
				//Creazione etichette
				Label lblSocial = new Label("UserName social:");
				Label lblPiattaforma = new Label("Piattaforma:");
				TextBox txtSocial = new TextBox();
				TextBox txtPiattaforma = new TextBox();
				//Aggiunta di una nuova riga, con relativi campi
				int r = gridSocial.getRowCount();
				gridSocial.insertRow(r);
				gridSocial.setWidget(r, 0, lblSocial);
				gridSocial.setWidget(r, 1, txtSocial);
				gridSocial.setWidget(r, 2, lblPiattaforma);
				gridSocial.setWidget(r, 3, txtPiattaforma);
			}
		};
				
		//Handler button per la rimozione di un social
		btnDelSocialHandler = new ClickHandler() {
			//Gestione evento onClick
			@Override
			public void onClick(ClickEvent event) {
				//Rimozione dell'ultima riga
				int row = gridSocial.getRowCount() - 1;
				gridSocial.removeRow(row);
			}	
		};
	}
}