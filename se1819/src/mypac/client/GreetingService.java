package mypac.client;


import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import mypac.shared.AccountSocial;
import mypac.shared.Allegato;
import mypac.shared.Categoria;
import mypac.shared.Domanda;
import mypac.shared.Risposta;
import mypac.shared.Utente;
import mypac.shared.UtenteGiudice;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {

	/**
	 * Metodo che permette di effettuare la registrazione
	 * @param nomeUtente
	 * @param password
	 * @param nome
	 * @param cognome
	 * @param email
	 * @param sesso
	 * @param dataNascita
	 * @param luogoNascita
	 * @param domicilio
	 * @param listaSocial
	 * @return messaggio di avvenuta (o meno) registrazione
	 */
	String effettuaRegistrazione(String nomeUtente, String password, String nome, String cognome, String email, char sesso, Date dataNascita,
			String luogoNascita, String domicilio, List<AccountSocial> listaSocial);

	/**
	 * Metodo che restituisce tutte le domande presenti nel sistema
	 * @return array delle domande
	 */
	Domanda[] restituisciDomande();

	/**
	 * Metodo che permette di effettuare il login
	 * @param nomeUtente
	 * @param password
	 * @return String di avvenuto (o meno) login
	 */
	String effettuaLogin(String nomeUtente, String password);


	/**
	 * Metodo che permette di effettuare il login
	 * @param d
	 * @param testoRisposta
	 * @param timestamp
	 * @param nomeUtente
	 * @return String di avvenuto inserimento
	 */
	String inserisciRisposta(Domanda d, String testoRisposta, Date timestamp, String nomeUtente);

	/**
	 * Metodo che permette di effettuare il login
	 * @return Array di categorie
	 */
	Categoria[] restituisciCategorie();

	/**
	 * Metodo che permette di effettuare una domanda
	 * @param testo
	 * @param categoria
	 * @param allegato
	 * @param risposte
	 * @param data
	 * @param nomeUtente
	 * @return String di avvenuto inserimento
	 */
	String effettuaDomanda(String testo, Categoria categoria, List<Allegato> allegato, List<Risposta> risposte,
			Date data, String nomeUtente);

	/**
	 * Metodo che permette di rinominare una categoria
	 * @param c
	 * @param nuovoNome
	 */
	void rinominaCategoria(Categoria c, String nuovoNome);

	/**
	 * Metodo che permette di inserire una domanda
	 * @param r
	 * @param voto
	 * @param nomeGiudice
	 * @return String di avvenuto inserimento
	 */
	String inserisciVoto(Risposta r, String voto, String nomeGiudice);

	/**
	 * Metodo che permette di leggere un voto associato ad una risposta
	 * @param risposta
	 * @return String di avvenuto inserimento
	 */
	String leggiVoto(Risposta risposta);

	/**
	 * Metodo che permette di controllare se un utente è giudice
	 * @param utenteConnesso
	 * @return boolean
	 */
	boolean isGiudice(String utenteConnesso);

	/**
	 * Metodo che permette di eliminare una risposta, data una domanda
	 * @param r
	 * @param d
	 * @return String di avvenuta rimozione
	 */
	String eliminaR(Risposta r, Domanda d);

	/**
	 * Metodo che permette l'inserimento di una nuova categoria
	 * @param nomeCategoria
	 * @return int dell'id della categoria
	 */
	int inserisciCategoria(String nomeCategoria);

	/**
	 * Metodo che permette di inserire una sottocategoriaa
	 * @param idPadre
	 * @param idFiglio
	 */
	void inserisciSottocategoria(int idPadre, int idFiglio);

	/**
	 * Metodo che permette di restituire la lista degli utenti registrati
	 * @return Array di Utente
	 */
	Utente[] restituisciUtenti();

	/**
	 * Metodo che permette l'eliminazione della domanda
	 * @param dom
	 */
	void eliminaDomanda(Domanda dom);

	/**
	 * Metodo che permette di nominare un utente, giudice
	 * @param nomeUtente
	 * @return String di avvenuta rinomina
	 */
	String nominaGiudice(String nomeUtente);

	/**
	 * Metodo che permette di restituire un UtenteGiudice dato il nome del giudice
	 * @param utenteConnesso
	 * @return UtenteGiudice
	 */
	UtenteGiudice getGiudice(String utenteConnesso);

	/**
	 * Metodo che permette di restituire un utente dato il nome dell'utente
	 * @param nomeUtente
	 * @return Utente
	 */
	Utente restituisciUtente(String nomeUtente);

	/**
	 * Metodo che permette di controllare se un utente è Utente
	 * @param utenteConnesso
	 * @return boolean
	 */
	boolean isUtente(String utenteConnesso);

	/**
	 * Metodo che permette di restituire i nome degli utenti non giudici
	 * @return Array di String
	 */
	String[] restituisciNomiUtentiNonGiudici();

	/**
	 * Metodo che restituisce la domanda (aggiornata) che ha lo stesso id
	 * della domanda (non aggiornata) passata come parametro
	 * @param domanda la domanda non aggiornata
	 * @return la domanda aggiornata
	 */
	Domanda restituisciDomanda(Domanda domanda);
}
