package mypac.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;

import mypac.shared.AccountSocial;
import mypac.shared.MyUtil;
import mypac.shared.Utente;

/***
 * 
 * Classe che implementa la schermata per visualizzare il tipo di profilo dell'utente loggato
 *
 */

public class Profilo {
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	/**
	 * Creazione dell'interfaccia mediante il popolamento del pannello principale
	 * @param panel
	 */
	public void onModuleLoad(Panel panel) {

		//Se l'utente connesso e' l'admin, il sistema lo informa
		if (Se1819.utenteConnesso.equalsIgnoreCase("admin")) {
			Window.alert("Sei l'admin.");
			return;
		}

		//Richiedo al server di restituire il tipo di utente
		greetingService.restituisciUtente(Se1819.utenteConnesso, new AsyncCallback<Utente>() {

			//Caso di fallimento
			@Override
			public void onFailure(Throwable caught) {
				//Messaggio d'errore
				Window.alert("Errore nella restituzione dell'utente" + caught.getMessage());
			}

			//Caso di successo
			@Override
			public void onSuccess(Utente result) {
				//Se l'utente non e' loggato, restituisco il messaggio d'errore
				if (result == null) {
					Window.alert("NomeUtente non presente o utente non loggato");
					//Redirect alla pagina di login
					panel.clear();
					Login l = new Login();
					l.onModuleLoad((VerticalPanel) panel);
					return;
				}

				//Griglia
				Grid griglia  = new Grid(9, 2);

				//Labels
				Label[] lbls = {
						new Label("Nome utente*:"),
						new Label("Password*:"),
						new Label("Email*:"),
						new Label("Nome:"),
						new Label("Cognome:"),
						new Label("Sesso:"),
						new Label("Data di nascita:"),
						new Label("Luogo di nascita:"),
						new Label("Luogo di domicilio o di residenza:")

				};
				//Info
				String[] info = {
						result.getNomeUtente(),
						result.getPassword(),
						result.getEmail(),
						result.getNome(),
						result.getCognome(),
						Character.toString(result.getSesso()),
						(result.getDataNascita()== null? "" : MyUtil.formattaTimestamp(result.getDataNascita())),
						result.getLuogoNascita(),
						result.getDomicilio()
				};
				//Popolamento griglia
				for (int r = 0; r< griglia.getRowCount(); r++) {
					griglia.setWidget(r, 0, lbls[r]);
					griglia.setWidget(r, 1, new Label(info[r]));
				}

				//Popolamento panel
				panel.add(new HTML("<h1>Profilo utente</h1>"));
				panel.add(griglia);
				panel.add(new HTML("<h3>Lista Account Social</h3"));

				//Ottengo gli account social
				List<AccountSocial> listaSocial = result.getListaSocial();

				//Se la lista dei social e' nulla, restituisce il messaggio che informa l'utente
				if (listaSocial == null || listaSocial.isEmpty())
					 panel.add(new Label("Non sono presenti account social"));
				else {
					//Altrimenti restituisce quanti account social ha
					Grid grigliaSocial = new Grid(listaSocial.size(), 4);

					for (int r=0; r<listaSocial.size(); r++) {
						AccountSocial a = listaSocial.get(r);
						grigliaSocial.setWidget(r, 0, new Label("Piattaforma:"));
						grigliaSocial.setWidget(r, 1, new Label(a.getPiattaforma()));
						grigliaSocial.setWidget(r, 2, new Label("Username:"));
						grigliaSocial.setWidget(r, 3, new Label(a.getUsername()));
					}

					//Popolamento panel
					panel.add(grigliaSocial);
				}
			}
		});
	}
}
