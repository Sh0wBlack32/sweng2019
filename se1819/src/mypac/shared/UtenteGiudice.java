package mypac.shared;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * Classe che implementa il giudice
 * 
 */
public class UtenteGiudice extends Utente implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Costruttore vuoto
	 */
	public UtenteGiudice() {
		super();
	}

	/**
	 * Costruttore
	 * @param nomeUtente
	 * @param password
	 * @param nome
	 * @param cognome
	 * @param email
	 * @param sesso
	 * @param dataNascita
	 * @param luogoNascita
	 * @param domicilio
	 * @param listaSocial
	 */
	public UtenteGiudice(String nomeUtente, String password, String nome, String cognome, String email, char sesso, Date dataNascita, String luogoNascita, String domicilio, List<AccountSocial>listaSocial) {
		super(nomeUtente, password, nome, cognome, email, sesso, dataNascita, luogoNascita, domicilio, listaSocial);
	}

	/**
	 * Costruttore
	 * @param utente
	 */
	public UtenteGiudice(Utente utente) {
		super.nomeUtente = utente.getNomeUtente();
		super.password = utente.getPassword();
		super.nome = utente.getNome();
		super.cognome = utente.getCognome();
		super.email = utente.getEmail();
		super.sesso = utente.getSesso();
		super.dataNascita = utente.dataNascita;
		super.luogoNascita = utente.getLuogoNascita();
		super.domicilio = utente.getDomicilio();
		super.listaSocial = utente.getListaSocial();
	}
}
