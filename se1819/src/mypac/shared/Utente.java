package mypac.shared;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

/**
 * 
 * Classe che implementa l'utente
 * 
 */
public class Utente implements Serializable {
	
	private static final long serialVersionUID = 1L;
	protected String nomeUtente;
	protected String password;
	protected String nome;
	protected String cognome;
	protected String email;
	protected char sesso;
	protected Date dataNascita;
	protected String luogoNascita;
	protected String domicilio;
	protected List<AccountSocial> listaSocial;
	
	/**
	 * Costruttore vuoto
	 */
	public Utente() {
		
	}
	
	/**
	 * Costruttore
	 * @param nomeUtente
	 * @param password
	 * @param email
	 */
	public Utente(String nomeUtente, String password, String email) {
		this.nomeUtente = nomeUtente;
		this.password = password;
		this.email = email;
	}
	
	/**
	 * Costruttore
	 * @param nomeUtente
	 * @param password
	 * @param nome
	 * @param cognome
	 * @param email
	 * @param sesso
	 * @param dataNascita
	 * @param luogoNascita
	 * @param domicilio
	 * @param listaSocial
	 */
	public Utente (String nomeUtente, String password, String nome, String cognome, String email, char sesso, Date dataNascita,
			String luogoNascita, String domicilio, List<AccountSocial> listaSocial) {
		this.nomeUtente = nomeUtente;
		this.password = password;
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.sesso = sesso;
		this.dataNascita = dataNascita;
		this.luogoNascita = luogoNascita;
		this.domicilio = domicilio;
		this.listaSocial = listaSocial;
	}

	/**
	 * Getter del nome utente
	 * @return nomeutente
	 */
	public String getNomeUtente() {
		return nomeUtente;
	}

	/**
	 * Getter della password
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter della password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Getter dell'email
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter dell'email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter del nome
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Setter del nome
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter del cognome
	 * @return cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * Setter del cognome
	 * @param cognome
	 */
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	/**
	 * Getter della data di nascita
	 * @return data di nascita
	 */
	public Date getDataNascita() {
		return dataNascita;
	}

	/**
	 * Setter della data di nascita
	 * @param dataNascita
	 */
	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	/**
	 * Getter del luogo di nascita
	 * @return luogo di nascita
	 */
	public String getLuogoNascita() {
		return luogoNascita;
	}

	/**
	 * Setter del luogo di nascita
	 * @param luogoNascita
	 */
	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	/**
	 * Getter del domicilio
	 * @return
	 */
	public String getDomicilio() {
		return domicilio;
	}

	/**
	 * Setter del domicilio
	 * @param domicilio
	 */
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	/**
	 * Getter del sesso
	 * @return sesso
	 */
	public char getSesso() {
		return sesso;
	}
	
	/**
	 * Getter della listaSocial
	 * @return listasocial
	 */
	public List<AccountSocial> getListaSocial() {
		return listaSocial;
	}

	/**
	 * Stampa l'utente come stringa
	 */
	@Override
	public String toString() {
		return "Utente: [nome utente = " + nomeUtente + ", password = " + password + ", email = " + email + ", nome = " + nome + ", cognome = " + cognome + ", sesso = " + sesso + 
				", data di nascita = " + dataNascita + ", luogo di nascita = " + luogoNascita + ", domicilio = " + domicilio + "]";
	}
}

