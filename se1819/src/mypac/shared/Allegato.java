package mypac.shared;
import java.io.Serializable;

/***
 * 
 * Classe che definisce l'allegato
 * 
 */

public class Allegato implements Serializable {
	private static final long serialVersionUID = 1L;
	private String url;
	
	/**
	 * Costruttore vuoto
	 */
	public Allegato() {
	}

	/**
	 * Costruttore
	 * @param url
	 */
	public Allegato(String url) {
		this.url = url;
	}

	/**
	 * Getter dell'url
	 * @return l'url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * Setter dell'url
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}