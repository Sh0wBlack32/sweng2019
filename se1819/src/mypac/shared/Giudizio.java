package mypac.shared;

import java.io.Serializable;

/**
 * 
 * Classe che implementa il giudizio del giudice
 * 
 */
public class Giudizio implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String voto;
	private UtenteGiudice giudice;

	/**
	 * Costruttore vuoto
	 */
	public Giudizio() {
	}
	
	/**
	 * Costruttore
	 * @param voto
	 * @param giudice
	 */
	public Giudizio(String voto, UtenteGiudice giudice) {
 		this.voto = voto;
 		this.giudice = giudice;
	}

	/**
	 * Getter del voto
	 * @return voto
	 */
 	public String getVoto() {
 		return voto;
 	}

 	/**
 	 * Setter del voto
 	 * @param voto
 	 */
 	public void setVoto(String voto) {
 		this.voto = voto;
 	}

 	/**
 	 * Getter del giudice
 	 * @return giudice
 	 */
 	public UtenteGiudice getGiudice() {
 		return giudice;
 	}

 	/**
 	 * Setter del giudice
 	 * @param giudice
 	 */
 	public void setGiudice(UtenteGiudice giudice) {
 		this.giudice = giudice;
 	}
}
