package mypac.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * Classe che rappresenta la risposta dell'utente
 * 
 */
public class Risposta implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private double id = 0.0;
	private static double cont = 0.0;
	private String testo;
	private Date timestamp;
	private List<Allegato> listaAllegati;
	private String voto = "" ;
	private String nomeUtente;
	private List<Giudizio> listaGiudizi;
	
	/**
	 * Costruttore vuoto
	 */
	public Risposta() {
	}

	/**
	 * Costruttore
	 * @param testo il testo della risposta
	 * @param timestamp il timestampa della risposta
	 */
	public Risposta(String testo, Date timestamp) {
		this.id = ++cont;
		this.testo = testo;
		this.timestamp = timestamp;
		this.listaGiudizi = new ArrayList<>();
		this.voto = "";
	}

	/**
	 * Costruttore
	 * @param testo	il testo della risposta
	 * @param timestamp	il timestamp della risposta
	 * @param nomeUtente il nome dell'utente che ha inserito la risposta
	 */
	public Risposta(String testo, Date timestamp, String nomeUtente) {
		this.id = ++cont;
		this.testo = testo;
		this.timestamp = timestamp;
		this.nomeUtente = nomeUtente;
		this.listaGiudizi = new ArrayList<>();
		this.voto = "";
	}
	
	/**
	 * Restituisce la lista degli allegati
	 * @return lista degli allegati
	 */
	public List<Allegato> getAllegati() {
		return listaAllegati;
	}

	/**
	 * Restituisce il testo della risposta
	 * @return testo della risposta
	 */
	public String getTesto() {
		return testo;
	}

	/**
	 * Inserisce il testo nella risposta
	 * @param testo il testo da inserire
	 */
	public void setTesto(String testo) {
		this.testo = testo;
	}

	/**
	 * Restituisce il voto della risposta
	 * @return il voto della risposta
	 */
	public String getVoto() {
		return voto;
	}

	/**
	 * Restituisce l'id della risposta
	 * @return l'id della risposta
	 */
	public double getId() {
		return id;
	}

	/**
	 * Restituisce il timestamp della risposta
	 * @return il timestamp della risposta
	 */
	public Date getTimestamp() {
		return timestamp;
	}
	
	/**
	 * Setta il timestamp della risposta
	 * @param timestamp 
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	/**
	 * Setta la lista degli allegati della risposta
	 * @param lista
	 */
	public void setListaAllegati(List<Allegato> lista) {
		this.listaAllegati =  lista;
	}
	
	/**
	 * Setta la risposta
	 * @param r
	 */
	public void setRisposta(Risposta r) {
		this.testo = r.getTesto();
		this.timestamp = r.getTimestamp();
		this.listaAllegati = r.getAllegati();
	}

	/**
	 * Restituisce il nome utente della risposta
	 * @return il nome utente
	 */
	public String getNomeUtente() {
		return nomeUtente;
	}

	/**
	 * Setta il nome utente della risposta
	 * @param nomeUtente
	 */
	public void setNomeUtente(String nomeUtente) {
		this.nomeUtente = nomeUtente;
	}

	/**
	 * Restituisce la lista dei giudizi associati alla risposta
	 * @return la lista dei giudizi 
	 */
	public List<Giudizio> getListaGiudizi() {
		return listaGiudizi;
	}

	/**
	 * Setta la lista dei giudizi
	 * @param listaGiudizi la lista dei giudizi
	 */
	public void setListaGiudizi(List<Giudizio> listaGiudizi) {
		this.listaGiudizi = listaGiudizi;
	}
	
	/**
	 * Aggiunge un giudizio alla risposta
	 * @param g giudizio da aggiungere
	 */
	public void addGiudizio(Giudizio g) {
		listaGiudizi.add(g);
		updateVoto();	
	}
	
	/**
	 * Rimuove un giudizio dalla risposta
	 * @param g il giudizio da rimuovere
	 */
	public void removeGiudizio(Giudizio g) {
		for(int i = 0; i < getListaGiudizi().size(); i++) {
			if(listaGiudizi.get(i).getGiudice().getNomeUtente().equalsIgnoreCase(g.getGiudice().getNomeUtente())) {
				this.listaGiudizi.remove(i);
			}
		}
	}
	
	/**
	 * Aggiorna il voto medio della risposta
	 */
	private void updateVoto() {
		int tempVoto = 0;
		for(int i = 0; i < listaGiudizi.size(); i++) {
			tempVoto += listaGiudizi.get(i).getVoto().length();
		}
		this.voto = convertVotoIntToString(tempVoto / listaGiudizi.size());
	}
	
	/**
	 * Converte il voto intero in stringa
	 * @param voto il voto intero
	 * @return il voto stringa
	 */
	private String convertVotoIntToString(int voto) {
		String temp = "";
		for(int i = 0; i < voto; i++) {
			temp += "*";
		}
		return temp;
	}
}
