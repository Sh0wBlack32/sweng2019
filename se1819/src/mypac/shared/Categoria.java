package mypac.shared;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/***
 * 
 * Classe che definisce la categoria
 * 
 */

public class Categoria implements Serializable {
	private static final long serialVersionUID = 1L;
	static int cont = 0;
	private int id;
	private String nomeCategoria;
	private Categoria padre;
	private List<Categoria> figli;

	/**
	 * Costruttore
	 */
	public Categoria() {
	}
	
	/**
	 * Costruttore
	 * @param nomeCategoria
	 */
	public Categoria(String nomeCategoria) {
		this.id = ++cont;
		this.nomeCategoria = nomeCategoria;
		this.padre = null;
		this.figli = new ArrayList<Categoria>();
	}
	
	/**
	 * Costruttore
	 * @param nomeCategoria
	 * @param figli
	 */
	public Categoria(String nomeCategoria, List<Categoria> figli) {
		this.id = ++cont;
		this.nomeCategoria = nomeCategoria;
		this.padre = null;
		this.figli = figli;
	}
	
	/**
	 * Getter dell'id
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter del nome categoria
	 * @return nomecategoria
	 */
	public String getNomeCategoria() {
		return nomeCategoria;
	}
	
	/**
	 * Getter della lista dei figli
	 * @return lista dei figli
	 */
	public List<Categoria> getFigli() {
		return figli;
	}
	
	/**
	 * Setter del nome della categoria
	 * @param nomeCategoria
	 */
	public void setCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	
	/**
	 * Setter della lista dei figli
	 * @param figli
	 */
	public void setFigli(List<Categoria> figli) {
		this.figli = figli;
	}
	
	/**
	 * Setter dell'id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Metodo che permette l'aggiunta di una sottocategoria
	 * @param f
	 */
	public void addFiglio(Categoria f) {
		figli.add(f);
		f.setPadre(this);
	}
	
	/**
	 * Setter del padre
	 * @param padre
	 */
	public void setPadre(Categoria padre) {
		this.padre = padre;
	}
	
	/**
	 * Getter del padre
	 * @return
	 */
	public Categoria getPadre() {
		return padre;
	}
	
	/**
	 * Metodo che verifica se una categoria e' una sottocategoria di un'altra
	 * @param c la sopracategoria
	 * @return true/false se questa categoria e'/non e' una sottocategoria dell'altra
	 */
	public boolean contenutaIn(Categoria c) {
		if (c == null) return true;
		//controlla se le due categorie hanno lo stesso nome
		if (this.getNomeCategoria().equals(c.getNomeCategoria())) return true;
		//altrimenti controlla se questa categoria ha lo stesso nome di uno dei figli
		//della categoria passata come parametro
		if (c.getFigli() != null && !c.getFigli().isEmpty())
			for (Categoria f: c.getFigli()) 
				if (this.contenutaIn(f)) return true;
		//se non ci sono corrispondenze ritorna false
		return false;
		
	}
	
	/**
	 * Setter del contatore
	 * @param cont
	 */
	@SuppressWarnings("static-access")
	public void setCont(int cont) {
		this.cont = cont;
	}
	
	/**
	 * Getter del contatore
	 * @return contatore
	 */
	public int getCont() {
		return cont;
	}
	
	
}