package mypac.shared;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * 
 * Classe che implementa la formattazione del timestamp e della data
 * 
 */
public class MyUtil {
	private static DateTimeFormat formatter;
	
	/**
	 * Metodo per formattare il time stamp
	 * @param timestamp
	 * @return timestamp formattato
	 */
	public static String formattaTimestamp(Date timestamp) {
		formatter = DateTimeFormat.getFormat("dd/MM/yy HH:mm");
		return formatter.format(timestamp);
	}
	
	/**
	 * Metodo per formattare la data
	 * @param data
	 * @return data formattata
	 */
	public static String formattaData(Date data) {
		formatter = DateTimeFormat.getFormat("dd/MM/yy");
		return formatter.format(data);
	}

}
