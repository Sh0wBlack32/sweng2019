package mypac.shared;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/***
 * 
 * Classe che definisce la domanda
 * 
 */

public class Domanda implements Serializable {
	private static final long serialVersionUID = 1L;
	private static double cont = 0.0;
	private double id = 0.0;
	private String testo;
	private Categoria categoria;
	private List<Allegato> listaAllegati;
	private Date timestamp;
	private List<Risposta> listaRisposte;
	private Utente utente;

	/**
	 * Costruttore vuoto
	 */
	public Domanda() {
	} 
	
	/**
	 * Costruttore
	 * @param testo
	 * @param categoria
	 * @param allegato
	 * @param risposte
	 * @param data
	 * @param utente
	 */
	public Domanda(String testo, Categoria categoria, List<Allegato> allegato, List<Risposta> risposte, Date data, Utente utente) {
		this.id = ++cont;
		this.testo = testo;
		this.categoria = categoria;
		this.listaAllegati = allegato;
		this.listaRisposte = risposte;
		this.timestamp = data;
		this.utente = utente;
	}
	
	/**
	 * Costruttore
	 * @param testo
	 */
	public Domanda(String testo) {
		this.testo = testo;
	}

	/**
	 * Getter del testo
	 * @return testo della domanda
	 */
	public String getTesto() {
		return testo;
	}
	
	/**
	 * Getter delle risposte
	 * @return lista delle risposte
	 */
	public List<Risposta> getRisposte() {
		return listaRisposte;
	}
	
	/**
	 * Setter delle risposte
	 * @param risposte
	 */
	public void setRisposte(List<Risposta> risposte) {
		this.listaRisposte = risposte;
	}
	
	/**
	 * Metodo che permette l'aggiunta di una risposta
	 * @param r
	 */
	public void addRisposta(Risposta r) {
		this.listaRisposte.add(r);
	}
	
	/**
	 * Getter della categoria
	 * @return la categoria
	 */
	public Categoria getCategoria() {
		return categoria;
	}

	/**
	 * Getter degli allegati
	 * @return lista allegati
	 */
	public List<Allegato> getAllegato() {
		return listaAllegati;
	}

	/**
	 * Getter dell'id
	 * @return id
	 */
	public double getId() {
		return id;
	}
	
	/**
	 * Setter del testo
	 * @param testo
	 */
	public void setTesto(String testo) {
		this.testo = testo;
	}
	
	/**
	 * Setter dell'id
	 * @param id
	 */
	public void setId(double id) {
		this.id = id;
	}
	
	/**
	 * Setter della categoria
	 * @param categoria
	 */
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	/**
	 * Setter degli allegati
	 * @param allegato
	 */
	public void setAllegato(List<Allegato> allegato) {
		this.listaAllegati = allegato;
	}
	
	/**
	 * Setter del timestamp
	 * @param timestamp
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	/**
	 * Getter del timestamp
	 * @return timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}
	
	/**
	 * Setter dell'utente
	 * @param utente
	 */
	public void setUtente(Utente utente) {
		this.utente = utente;
	}
	
	/**
	 * Getter dell'utente
	 * @return utente
	 */
	public Utente getUtente() {
		return utente;
	}
	
	/**
	 * Metodo che permetta la rimoziona di una risposta
	 * @param r
	 */
	public void removeRisposta(Risposta r) {
		int index = 0;
		boolean trovato = false;
		while (!trovato && index < listaRisposte.size()) {
			if (listaRisposte.get(index).getId() == r.getId()) {
				trovato = true;
			}
			else index++;
		}
		if (trovato) listaRisposte.remove(index);
	}
}
