package mypac.shared;

import java.io.Serializable;

/***
 * 
 * Classe che definisce l'account social dell'utente
 *
 */

public class AccountSocial  implements Serializable{
	private static final long serialVersionUID = 1L;
	String username;
	String piattaforma;
	
	/**
	 * Costruttore vuoto
	 */
	public AccountSocial(){
	};
	
	/**
	 * Costruttore
	 * @param username 
	 * @param piattaforma 
	 */
	public AccountSocial(String username, String piattaforma) {
		this.username = username;
		this.piattaforma = piattaforma;
	}
	
	/**
	 * Restituisce l'username
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Setta l'username
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Getter della piattaforma
	 * @return la piattaforma
	 */
	public String getPiattaforma() {
		return piattaforma;
	}

	/**
	 * Setter della piattaforma
	 * @param piattaforma
	 */
	public void setPiattaforma(String piattaforma) {
		this.piattaforma = piattaforma;
	}

	/**
	 * Stampa una stringa che rappresenta l'account
	 */
	@Override
	public String toString() {
		return "AccountSocial [username: " + username + ", piattaforma: " + piattaforma + "]";
	}

}
