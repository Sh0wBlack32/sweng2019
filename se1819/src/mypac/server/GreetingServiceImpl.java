package mypac.server;

import mypac.client.GreetingService;
import mypac.shared.AccountSocial;
import mypac.shared.Allegato;
import mypac.shared.Domanda;
import mypac.shared.Giudizio;
import mypac.shared.Risposta;
import mypac.shared.Categoria;
import mypac.shared.Utente;
import mypac.shared.UtenteGiudice;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {

	private DB db;
	private Map<String, Utente> utenti;
	private Map<String, UtenteGiudice> giudici;
	private Map<Double, Domanda> domande;
	private Map<Double, Risposta> risposte;
	private Map<Integer, Categoria> categorie;

	@SuppressWarnings("deprecation")
	public void init() {
		//Creazione DB
		db = DBMaker.newFileDB(new File("swengDb.db")).make();

		//Creazione tree map utente
		utenti = db.getTreeMap("utenti");
		//Creazione tree map utente
		giudici = db.getTreeMap("giudici");
		//Creazione tree map domande
		domande = db.getTreeMap("domande");
		//Creazione tree map categorie
		categorie = db.getTreeMap("categorie");

		//popolamento categorie
		Categoria[] categoriePredefinite = {
				new Categoria("Ambiente"),
				new Categoria("Animali"),
				new Categoria("Arte e cultura"),
				new Categoria("Elettronica e tecnologia"),
				new Categoria("Sport"),
				new Categoria("Svago"),
				new Categoria("Calcio"),
				new Categoria("Serie A")
		};

		for (Categoria c : categoriePredefinite) {
			categorie.put(c.getId(), c);
		}
		//Aggiunta di sottocategorie
		inserisciSottocategoria(5,7);
		inserisciSottocategoria(7,8);
	//	categorie.get(5).addFiglio(categorie.get(7));
	//	categorie.get(7).addFiglio(categorie.get(8));

		//Creazione di due risposte generate dal "System" come test
		Risposta r1 = new Risposta("Boh! Secondo me 4", new Date("05/19/2019"), "System");
		Risposta r2 = new Risposta("Ma dai, sicuramente 4!!!", new Date("04/01/2019"), "System");

		//Inserimento dei nuovi utenti nella struttura dati del DB
		utenti.put("Alby92", new Utente("Alby92", "a", "a@mail.it"));
		utenti.put("BeaZZZ", new Utente("BeaZZZ", "b", "b@mail.it"));
		utenti.put("Mighi", new Utente("Mighi", "c", "m@mail.it"));
		utenti.put("Dave", new Utente("Dave", "d", "d@mail.it"));
		//Inserimento dei giudici nella struttura dati del DB
		giudici.put("Mighi", new UtenteGiudice(utenti.get("Mighi")));
		giudici.put("Dave", new UtenteGiudice(utenti.get("Dave")));

		//Creazione di un giudizio legato alla risposta r1
		r1.addGiudizio(new Giudizio("***", giudici.get("Mighi")));
		//Creazione di una lista di tipo "Risposta"
		List<Risposta> listaRisp = new ArrayList<Risposta>();
		//Inserimento delle due risposte create, nella lista
		listaRisp.add(r1);
		listaRisp.add(r2);

		//Creazione tree map relativo alle risposte
		risposte = db.getTreeMap("risposte");
		//Inserimento delle risposte nel DB, con key = all'id di ogni risposta
		risposte.put(r1.getId(), r1);
		risposte.put(r2.getId(), r2);

		//Inserimento domande di test
		Domanda d1 = new Domanda("Quante gambe ha una cavalletta??", categorie.get(2),  null, listaRisp, new Date("06/19/2019"), utenti.get("Alby92"));
		Domanda d2 = new Domanda("Il verso dell'ippopotamo?", categorie.get(2), null, new ArrayList<Risposta>(), new Date("06/14/2019"), utenti.get("Alby92"));
		Domanda d3 = new Domanda("Paddle nuovo sport piu' giocato?", categorie.get(5), null, new ArrayList<Risposta>(), new Date("12/24/2018"), utenti.get("BeaZZZ"));
		Domanda d4 = new Domanda("Cristiano Ronaldo, futura promessa?", categorie.get(7), null, new ArrayList<Risposta>(), new Date("06/23/2019"), utenti.get("Alby92"));
		Domanda d5 = new Domanda("Il Bologna e scudetto ???", categorie.get(8), null, new ArrayList<Risposta>(), new Date("10/24/2018"), utenti.get("BeaZZZ"));

		//Inserimento domande nel tree map
		domande.put(d1.getId(), d1);
		domande.put(d2.getId(), d2);
		domande.put(d3.getId(), d3);
		domande.put(d4.getId(), d4);
		domande.put(d5.getId(), d5);

	}

	/**
	 * Metodo che permette di effettuare la registrazione
	 * @param nomeUtente
	 * @param password
	 * @param nome
	 * @param cognome
	 * @param email
	 * @param sesso
	 * @param dataNascita
	 * @param luogoNascita
	 * @param domicilio
	 * @param listaSocial
	 * @return messaggio di avvenuta (o meno) registrazione
	 */
	public String effettuaRegistrazione(String nomeUtente, String password, String nome, String cognome, String email, char sesso, Date dataNascita,
			String luogoNascita, String domicilio, List<AccountSocial> listaSocial)  throws IllegalArgumentException {
		List<AccountSocial> lista = listaSocial;
		Utente u = new Utente(nomeUtente, password, nome, cognome, email, sesso, dataNascita, luogoNascita, domicilio, lista);

		return (utenti.putIfAbsent(nomeUtente, u) == null) ? "Registrazione effettuata" : "Username gia' presente";

	}

	@Override
	public Domanda[] restituisciDomande() {
		Domanda[] listaDomande = new Domanda[domande.size()];
		int i = 0;
		for (Map.Entry<Double, Domanda> entry : domande.entrySet()) {
			listaDomande[i] = entry.getValue();
			i++;
		}
		return listaDomande;
	}


	/**
	 * Metodo che permette di effettuare il login.
	 */
	public String effettuaLogin(String nomeUtente, String password) throws IllegalArgumentException {
		String result;
		if(nomeUtente.equals("admin") && password.equals("admin"))
			result = "Login Amministratore effettuato";
		else {
			boolean isExists = utenti.get(nomeUtente) == null ? false : true;

			result = (isExists && utenti.get(nomeUtente).getPassword().equals(password)) ?
					"Login effettuato" : "Login errato";
			/*if(result.equalsIgnoreCase("Login effettuato"))
				Se1819.utenteConnesso = utenti.get(nomeUtente).getNomeUtente();*/
		}
		return result;
	}

	/**
	 * Metodo che permette di effettuare il login.
	 */
	@Override
	public String inserisciRisposta(Domanda d, String testoRisposta, Date timestamp, String nomeUtente) {
		Risposta r = new Risposta(testoRisposta, timestamp, nomeUtente);
		risposte.put(r.getId(), r);
		domande.get(d.getId()).addRisposta(r);
		return "Risposta aggiunta";
	}

	@Override
	public Categoria[] restituisciCategorie() {
		Categoria[] listaCategorie = new Categoria[categorie.size()];
		int i = 0;
		for (Map.Entry<Integer, Categoria> entry : categorie.entrySet()) {
			listaCategorie[i] = entry.getValue();
			i++;
		}
		return listaCategorie;
	}


	@Override
	public String effettuaDomanda(String testo, Categoria categoria, List<Allegato> allegato, List<Risposta> risposte,
			Date data, String nomeUtente) {

		Domanda d = new Domanda(testo, categorie.get(categoria.getId()), allegato, risposte, data, utenti.get(nomeUtente));
		domande.put(d.getId(), d);

		return ("Domanda inserita con successo!");
}

	@Override
	public void rinominaCategoria(Categoria c, String nuovoNome) {
		categorie.get(c.getId()).setCategoria(nuovoNome);
	}

	@Override
	public int inserisciCategoria(String nomeCategoria) {
		Categoria c = new Categoria(nomeCategoria);
		categorie.put(c.getId(), c);
		return c.getId();
	}

	@Override
	public void inserisciSottocategoria(int idPadre, int idFiglio) {
	/*	Categoria c = categorie.get(idPadre);
		c.addFiglio(categorie.get(idFiglio));
		categorie.categorie.replace(idPadre, c);
		*/
		categorie.get(idPadre).addFiglio(categorie.get(idFiglio));
		categorie.get(idFiglio).setPadre(categorie.get(idPadre));
	}

	@Override
	public String inserisciVoto(Risposta r, String voto, String nomeGiudice) {
		UtenteGiudice giudice = getGiudice(nomeGiudice);
		risposte.get(r.getId()).addGiudizio(new Giudizio(voto, giudice));
		return "Voto inserito";
	}

	@Override
	public String leggiVoto(Risposta r) {
		String voto = risposte.get(r.getId()).getVoto();
		return voto != null ? voto : "";
	}

	@Override
	public boolean isGiudice(String utenteConnesso) {
		return giudici.containsKey(utenteConnesso);
	}


	@Override
	public boolean isUtente(String utenteConnesso) {
		return utenti.containsKey(utenteConnesso);
	}

	@Override
	public UtenteGiudice getGiudice(String utenteConnesso) {
		return giudici.get(utenteConnesso);
	}

	@Override
	public String eliminaR(Risposta r, Domanda d) {
		domande.get(d.getId()).removeRisposta(r);
		risposte.remove(r.getId());
		return "Risposta eliminata con successo";
	}

	@Override
	public Utente[] restituisciUtenti() {
		Utente[] listaUtenti = new Utente[utenti.size()];
		int i = 0;
		for (Map.Entry<String, Utente> entry : utenti.entrySet()) {
			listaUtenti[i] = entry.getValue();
			i++;
		}
		return listaUtenti;
	}

	@Override
	public String nominaGiudice(String nomeUtente) {
		//Crea un nuovo giudice g
		Utente u = utenti.get(nomeUtente);
		UtenteGiudice g = new UtenteGiudice(u);
		giudici.put(nomeUtente, g);
		return("L'utente e' stato nominato giudice con successo!");
	}

	@Override
	public void eliminaDomanda(Domanda dom) {
		domande.remove(dom.getId()); /*DA SISTEMARE, DA' ERRORE PERCHE' NON ESISTE METODO GET ID*/
	}

	@Override
	public Utente restituisciUtente(String nomeUtente) {

		return utenti.get(nomeUtente);
	}

	@Override
	public String[] restituisciNomiUtentiNonGiudici() {
		List<String> listaKey = new ArrayList<String>();
		utenti.keySet().stream().forEach(k -> {
			if (!giudici.containsKey(k))
				listaKey.add(k);
		});

		return listaKey.toArray(new String[0]);
	}


	@Override
	public Domanda restituisciDomanda(Domanda domanda) {
		return domande.get(domanda.getId());
	}

}
