package se1819;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;

import com.google.gwt.junit.client.GWTTestCase;

import mypac.server.GreetingServiceImpl;
import mypac.shared.AccountSocial;
import mypac.shared.Categoria;
import mypac.shared.Domanda;
import mypac.shared.Risposta;
import mypac.shared.Utente;
import mypac.shared.UtenteGiudice;

/**
 * Classe che permette di effettuare i test sul server
 *
 */
class Test extends GWTTestCase{
	static GreetingServiceImpl myService;

	//inizializzazione 
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		myService = new GreetingServiceImpl();
		myService.init();
	}


	/**
	 * Verifica che il metodo init() inizializzi il db
	 */
	@org.junit.jupiter.api.Test
	final void testInit() {
		assertNotNull(myService.restituisciUtenti());
		assertNotNull(myService.restituisciCategorie());
		assertNotNull(myService.restituisciDomande());
	}

	@SuppressWarnings("deprecation")
	@org.junit.jupiter.api.Test
	/**
	 * Verifica che il metodo "EffettuaRegistrazione" inserisca nuovi utenti nel db
	 */
	final void testEffettuaRegistrazione() {
		//inserisco un nuovo utente con tutti i campi
		assertEquals("Registrazione effettuata", myService.effettuaRegistrazione(
				"luca572", "passw0rd", "Luca", "Bianchi", "lucabianchi@gmail.com", 'M', new Date("05/08/1990"), "Bologna", "Bologna", new ArrayList<AccountSocial>()));
		//inserisco due utenti con lo stesso username
		assertEquals("Registrazione effettuata", myService.effettuaRegistrazione(
				"maria235", "pAssword", "Maria", "Verdi", "mariaVerdi@boh.it", 'F', new Date("06/03/1970"), "Bologna", "Reggio Emilia", null));
		assertEquals("Username gia' presente", myService.effettuaRegistrazione(
				"maria235", "paSSword", "Marilu", "Rossi", "", 'F', new Date("04/02/1950"), "Bologna", "Modena", null));
		//l'utente inserito e' il primo
		assertEquals("Maria", myService.restituisciUtente("maria235").getNome());

	}

	/**
	 * Verifica che il metodo "RestituisciDomande" restituisca le domande
	 * presenti nel db
	 */
	@org.junit.jupiter.api.Test
	final void testRestituisciDomande() {
		assertTrue(Arrays.stream(myService.restituisciDomande())
				.allMatch(d -> d instanceof Domanda));
	}
	
	/**
	 * Verifica che il metodo "effettuaLogin" permetta l'accesso
	 * alla piattaforma solo per gli utenti registrati e l'admin
	 */
	@org.junit.jupiter.api.Test
	final void testEffettuaLogin() {
		assertEquals("Login Amministratore effettuato", myService.effettuaLogin("admin", "admin"));
		assertEquals("Login effettuato", myService.effettuaLogin("Alby92", "a"));
		assertEquals("Login errato", myService.effettuaLogin("Alby92", "b"));
	}

	/**
	 * Verifica che il metodo "InserisciRisposta" inserisca correttamente una risposta nel db
	 */
	@org.junit.jupiter.api.Test
	final void testInserisciRisposta() {
		Domanda d = myService.restituisciDomande()[0];
		String testoRisposta = "Sono una risposta alla prima domanda del db";
		Date timestamp = new Date();
		String nomeUtente = myService.restituisciUtenti()[0].getNomeUtente();
		assertEquals("Risposta aggiunta",
				myService.inserisciRisposta(d, testoRisposta, timestamp, nomeUtente));

	}

	/**
	 * Verifica che il metodo "RestituisciCategorie" restituisca categorie
	 */
	@org.junit.jupiter.api.Test
	final void testRestituisciCategorie() {
		assertTrue(Arrays.stream(myService.restituisciCategorie())
				.allMatch(c -> c instanceof Categoria));
	}

	/**
	 * Verifica che il metodo "EffettuaDomanda" inserisca una nuova domanda nel db
	 */
	@org.junit.jupiter.api.Test
	final void testEffettuaDomanda() {
		//dati della domanda
		String testo = "Sono una nuova domanda sull'ambiente??";
		Categoria categoria = myService.restituisciCategorie()[0];
		String utente = myService.restituisciUtenti()[0].getNomeUtente();
		assertEquals("Domanda inserita con successo!",
				myService.effettuaDomanda(testo, categoria, null, null, new Date(), utente));

	}

	/**
	 * Verifica che il metodo "RinominaCategoria" permetta di modificare
	 * il nome di una categoria esistente
	 */
	@org.junit.jupiter.api.Test
	final void testRinominaCategoria() {
		String nuovoNome = "nome di prova";
		Categoria c = myService.restituisciCategorie()[0];
		//il nome attuale e' diverso dal nuovo
		assertNotEquals(nuovoNome, c.getNomeCategoria());
		//rinomino la categoria
		myService.rinominaCategoria(c, nuovoNome);
		//ora il nome attuale e' uguale al nuovo nome
		assertEquals(nuovoNome, myService.restituisciCategorie()[0].getNomeCategoria());

	}

	/**
	 * Verifica che il metodo "InserisciCategoria" permetta
	 * l'inserimento di una nuova categoria nel db
	 */
	@org.junit.jupiter.api.Test
	final void testInserisciCategoria() {
		String nomeCategoria = "La mia nuova categoria";
		//prima la categoria NON e' presente
		assertFalse(Arrays.stream(myService.restituisciCategorie())
				.anyMatch(c -> c.getNomeCategoria().equals(nomeCategoria)));
		myService.inserisciCategoria(nomeCategoria);
		//dopo, la categoria e' presente
		assertTrue(Arrays.stream(myService.restituisciCategorie())
				.anyMatch(c -> c.getNomeCategoria().equals(nomeCategoria)));
	}

	/**
	 * Verifica che il metodo "InserisciSottocategoria" permetta
	 * l'inserimento di una nuova sottocategoria nel db
	 */
	@org.junit.jupiter.api.Test
	final void testInserisciSottocategoria() {
		//prima, il padre NON contiene il figlio
		Categoria padre = myService.restituisciCategorie()[0];
		Categoria figlio = myService.restituisciCategorie()[1];
		assertEquals(false, padre.getFigli().contains(figlio));
		//dopo, la sottocategoria e' stata inserita
		myService.inserisciSottocategoria(1, 2);
		padre = myService.restituisciCategorie()[0];
		figlio = myService.restituisciCategorie()[1];
		assertEquals(true, padre.getFigli().contains(figlio));
		assertEquals(padre, figlio.getPadre());

	}

	/**
	 * Verifica che il metodo "InserisciVoto" permetta
	 * l'inserimento di un voto relativo ad una risposta nel db
	 */
	@SuppressWarnings("unused")
	@org.junit.jupiter.api.Test
	final void testInserisciVoto() {
		Risposta r = myService.restituisciDomande()[0].getRisposte().get(0);
		int size = r.getListaGiudizi().size();
		if (r == null) fail();
		myService.inserisciVoto(r, "****", "d");
		int newSize = myService.restituisciDomande()[0].getRisposte().get(0).getListaGiudizi().size();
		//dopo l'inserimento del voto, la lista contiene un elemento in piu'
		assertEquals(size+1, newSize);
	}

	/**
	 * Verifica che il metodo "leggiVoto" restituisca il voto
	 * associato ad una risposta
	 */
	@org.junit.jupiter.api.Test
	final void testLeggiVoto() {		
		Risposta r = myService.restituisciDomande()[0].getRisposte().get(0);
		assertEquals("***", r.getListaGiudizi().get(0).getVoto());
		
	}

	/**
	 * Verifica che il metodo "IsGiudice" restituisca true (false) 
	 * se l'utente e' (non e') un giudice
	 */
	@org.junit.jupiter.api.Test
	final void testIsGiudice() {
		assertFalse(myService.isGiudice("Alby92"));
		assertTrue(myService.isGiudice("Mighi"));
	}

	/**
	 * Verifica che il metodo "IsUtente" restituisca true (false) 
	 * 
	 */
	@org.junit.jupiter.api.Test
	final void testIsUtente() {
		assertFalse(myService.isUtente("admin"));
		assertTrue(myService.isUtente("Alby92"));
	}


	/**
	 * Verifica che il metodo "getGiudice" restituisca
	 * il giudice richiesto
	 */
	@org.junit.jupiter.api.Test
	final void testGetGiudice() {
		UtenteGiudice g = myService.getGiudice("Mighi");
		assertNotNull(g);
		assertEquals("Mighi", g.getNomeUtente());
	}


	/**
	 * Verifica che il metodo "eliminaRisposta" elimini
	 * correttamente una risposta dal db
	 */
	@org.junit.jupiter.api.Test
	final void testEliminaR() {
		//prende la prima domanda che presenta delle risposte, se esiste
		Optional<Domanda> od = Arrays.stream(myService.restituisciDomande()).filter(d -> !d.getRisposte().isEmpty()).findFirst();
		if (od.isPresent()) {
			Domanda d = od.get();
			Risposta r = d.getRisposte().get(0);
			//prima la domanda contiene la risposta
			assertTrue(d.getRisposte().contains(r));
			myService.eliminaR(r, d);
			od = Arrays.stream(myService.restituisciDomande()).filter(dom -> dom.getId() == d.getId()).findFirst();
			//dopo la domanda non contiene piu' la risposta
			assertFalse(od.get().getRisposte().contains(r));//get(0).getId() == r.getId());
		}
		else fail();
	}

	/**
	 * Verifica che il metodo "RestituisciUTenti"
	 * restituisca degli utenti
	 */
	@org.junit.jupiter.api.Test
	final void testRestituisciUtenti() {
		assertTrue(Arrays.stream(myService.restituisciUtenti())
				.allMatch(u -> u instanceof Utente));
	}

	/**
	 * Verifica che il  metodo "NominaGiudice" permetta
	 * di promuovere un utente registrato in un utente giudice
	 */
	@org.junit.jupiter.api.Test
	final void testNominaGiudice() {
		String nomeUtente = myService.restituisciNomiUtentiNonGiudici()[0];
		//prima l'utente NON e' un giudice
		assertFalse(myService.isGiudice(nomeUtente));
		myService.nominaGiudice(nomeUtente);
		//dopo l'utente e' un giudice
		assertTrue(myService.isGiudice(nomeUtente));
	}

	/**
	 * Verifica che il metodo "eliminaDomanda" permetta
	 * di eliminare una domanda dal db
	 */
	@org.junit.jupiter.api.Test
	final void testEliminaDomanda() {
		Domanda dom  = myService.restituisciDomande()[1];
		myService.eliminaDomanda(dom);
		//verifica che la domanda non sia piu contenuta
		assertFalse(Arrays.stream(myService.restituisciDomande()).anyMatch(d -> d.getId()== dom.getId()));
		
	}

	/** 
	 * Verifica che il metodo "RestituisciUtente" restituisca
	 * l'utente richiesto
	 */
	@org.junit.jupiter.api.Test
	final void testRestituisciUtente() {
		Utente u = myService.restituisciUtente("Alby92");
		assertNotNull(u);
		assertEquals("Alby92", u.getNomeUtente());
	}

	/**
	 * Verifica che il metodo "RestituisciNomiUtentiNonGiudici" 
	 * restituisca l'elenco degli utenti registrati ma non giudici
	 */
	@org.junit.jupiter.api.Test
	final void testRestituisciNomiUtentiNonGiudici() {
		Utente[] utenti = myService.restituisciUtenti();
		for (Utente u: utenti) {
			//true se l'utente NON e' giudice
			boolean b1 = !myService.isGiudice(u.getNomeUtente());
			//true se l'utente e' presenta nella lista da restituire
			boolean b2 = Arrays.stream(myService.restituisciNomiUtentiNonGiudici()).anyMatch(s -> s.equals(u.getNomeUtente()));
			assertEquals(b1, b2);
		}
	}
	
	/**
	 * Verifica che il metodo "Restituisci domanda"
	 * restituisca la domanda corretta
	 */
	@org.junit.jupiter.api.Test
	final void testRestituisciDomanda() {
		Domanda d = myService.restituisciDomande()[0];
		assertEquals(d.getId(), myService.restituisciDomanda(d).getId());
	}
	
	@Override
	public String getModuleName() {
		// TODO Auto-generated method stub
		return null;
	}
}
