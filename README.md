﻿﻿﻿﻿﻿##Progetto di ingegneria del software 2018/2019
Alberto Zini - Michele Vece - Beatrice Spiga
 
Repository del progetto di ingegneria del software, il quale consiste nella creazione di un sito web che permetta agli utenti iscritti di porre domande e di fornire risposte agli interrogativi proposti. 
L'applicazione permette all'utente di registrarsi al sito, loggarsi, inserire una nuova domanda, rispondere ad una domanda, visualizzare domande e risposte, dare un giudizio ad una risposta data da un altro utente (funzionalità disponibile solo per gli
utenti “giudici”).

Nella repository sono presenti i codici sorgenti dell'applicazione web.